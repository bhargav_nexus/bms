﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using PetaPoco;
using System.ComponentModel.DataAnnotations;
using bms.Repository;
using System.Linq;
using bms.Models;
using bms.ViewModels;
namespace bms.Repository
{
    public class productsRepo : BaseConnection
    {
        public bool IsSuccess { get; set; }
        public string Msg { get; set; }

        #region Products
        public int InsertProduct(products model)
        {
            int RetVal = 0;
            try
            {
                RetVal = Convert.ToInt32(Connection.Insert("products", "productid", model));
                this.Msg = "Record Saved Successfully !!!";
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                Msg = ex.Message;
            }
            return RetVal;
        }

        public int UpdateProduct(products model)
        {
            int RetVal = 0;
            try
            {
                RetVal = Convert.ToInt32(Connection.Update("products", "productid", model, model.productid));
                this.Msg = "Record Updated Successfully !!!";
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                Msg = ex.Message;
            }
            return RetVal;
        }

        public List<products> GetAllProduct()
        {
            List<products> ItemList = new List<products>();
            try
            {
                Sql qryText = Sql.Builder.Append("SELECT * FROM products");
                //Append For Long Query, Remove comment from below line
                //qryText.Append("");
                ItemList = Connection.Fetch<products>(qryText);
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                Msg = ex.Message;
            }
            return ItemList;
        }

        public List<ProductsViewModel> GetPagedProduct(int page, out long ttlRec, int pagesize, int cateid, int tagid, string product)
        {
            ttlRec = 0;
            List<ProductsViewModel> ItemList = new List<ProductsViewModel>();
            try
            {
                Sql qryText = Sql.Builder.Append("select products.*,productcatname from products  inner join productcatmapping on productid=productcatmapping.prdctid inner join productcategory on productcatid=productcatmapping.catid where 1=1");
                if (cateid > 0)
                { qryText.Append("and catid=" + cateid); }
                if (!String.IsNullOrEmpty(product))
                { qryText.Append("and productname like '%" + product + "%'"); }
                if (tagid > 0)
                { qryText.Append("and tagid=" + tagid); }
                //Append For Long Query, Remove comment from below line
                //qryText.Append("");
                var data = Connection.Page<ProductsViewModel>(page, pagesize, qryText);
                ItemList = data.Items.ToList<ProductsViewModel>();
                ttlRec = data.TotalItems;
            }
            catch (Exception ex)
            {
                Msg = ex.Message;
            }
            return ItemList;
        }

        //Get record by Primary Key
        public products GetProductById(int PK)
        {
            products itemProp = new products();
            try
            {
                Sql qryText = Sql.Builder.Append("SELECT productid,productname,productshortdesc,productdesc,catid FROM products inner join productcatmapping on prdctid=productid WHERE productid = @0", PK);
                //Append For Long Query, Remove comment from below line
                //qryText.Append("");
                itemProp = Connection.Single<products>(qryText);
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                Msg = ex.Message;
            }
            return itemProp;
        }

        //Delete record by Primary Key
        public int DeleteProduct(int PK)
        {
            int RetVal = 0;
            try
            {
                RetVal = Connection.Delete<products>("WHERE productid = @0", PK);
                this.Msg = "Record Deleted Successfully !!!";
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                Msg = ex.Message;
            }
            return RetVal;
        }


        #endregion

        #region ProductImages
        public int InsertProductImage(productimages model)
        {
            int RetVal = 0;
            try
            {
                RetVal = Convert.ToInt32(Connection.Insert("productimages", "productimgid", model));
                this.Msg = "Record Saved Successfully !!!";
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                Msg = ex.Message;
            }
            return RetVal;
        }
        internal List<productimages> GetProductImgsById(int p)
        {
            List<productimages> prdcts = new List<productimages>();
            Sql qryTxt = Sql.Builder.Append("select * from productimages where productid=" + p);
            prdcts = Connection.Fetch<productimages>(qryTxt);
            return prdcts;
        }
        internal productimages GetProductImgById(int imgid)
        {
            productimages prdct = new productimages();
            Sql qryTxt = Sql.Builder.Append("select * from productimages where productimgid=" + imgid);
            prdct = Connection.SingleOrDefault<productimages>(qryTxt);
            return prdct;
        }
        internal int DeleteProductImg(int imgid)
        {
            Sql qry = Sql.Builder.Append("delete  from productimages where productimgid=" + imgid);
            return Connection.Execute(qry);
        }
        #endregion

        #region Category
        public int AddCategory(productcategory model)
        {
            int RetVal = 0;
            try
            {
                Sql qryTxt = Sql.Builder.Append("select count(*) from productcategory where productcatname='" + model.productcatname + "'");
                if (Connection.Single<int>(qryTxt) == 0)
                {
                    RetVal = Convert.ToInt32(Connection.Insert("productcategory", "productcatid", model));
                    this.Msg = "Record Saved Successfully !!!";
                    IsSuccess = true;
                }
                else
                {
                    Msg = "Category is already added";
                }
            }
            catch (Exception ex)
            {
                Msg = ex.Message;
            }
            return RetVal;
        }
        public int DeleteCategory(int PK)
        {
            int RetVal = 0;
            try
            {
                RetVal = Connection.Delete<productcategory>("WHERE productcatid = @0", PK);
                this.Msg = "Record Deleted Successfully !!!";
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                Msg = ex.Message;
            }
            return RetVal;
        }
        public int ChangeCatStatus(int CatId)
        {
            try
            {
                Sql qryTxt = Sql.Builder.Append("update productcategory set catstatus=(select case when catstatus=1 then 0 else 1 end from productcategory where productcatid=" + CatId + ") where productcatid=" + CatId);
                return Connection.Execute(qryTxt);
            }
            catch (Exception ex)
            {
                Msg = ex.Message;
                return 0;
            }
        }
        /// <summary>
        /// get category, 
        /// </summary>
        /// <param name="sts">true if only get active category</param>
        /// <returns></returns>
        public List<productcategory> GetCategory(bool sts)
        {
            List<productcategory> ItemList = new List<productcategory>();
            try
            {
                Sql qryText = Sql.Builder.Append("SELECT * FROM productcategory");
                if (sts)
                {
                    qryText.Append("where catstatus=1");
                }
                //Append For Long Query, Remove comment from below line
                //qryText.Append("");
                ItemList = Connection.Fetch<productcategory>(qryText);
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                Msg = ex.Message;
            }
            return ItemList;
        }
        #endregion

        #region Tags
        public int AddTag(producttags model)
        {
            int RetVal = 0;
            try
            {
                Sql qryTxt = Sql.Builder.Append("select count(*) from producttags where tagname='" + model.tagname + "'");
                if (Connection.Single<int>(qryTxt) == 0)
                {
                    RetVal = Convert.ToInt32(Connection.Insert("producttags", "tagid", model));
                    this.Msg = "Record Saved Successfully !!!";
                    IsSuccess = true;
                }
                else
                {
                    Msg = "Tag is already added";
                }
            }
            catch (Exception ex)
            {
                Msg = ex.Message;
            }
            return RetVal;
        }
        public int DeleteTag(int PK)
        {
            int RetVal = 0;
            try
            {
                RetVal = Connection.Delete<producttags>("WHERE tagid = @0", PK);
                this.Msg = "Record Deleted Successfully !!!";
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                Msg = ex.Message;
            }
            return RetVal;
        }
        public List<producttags> GetTag()
        {
            List<producttags> ItemList = new List<producttags>();
            try
            {
                Sql qryText = Sql.Builder.Append("SELECT * FROM producttags");
                //Append For Long Query, Remove comment from below line
                //qryText.Append("");
                ItemList = Connection.Fetch<producttags>(qryText);
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                Msg = ex.Message;
            }
            return ItemList;
        }
        internal List<int> GetTagsByPId(int p)
        {
            Sql qryTxt = Sql.Builder.Append("select tagid from producttagmapping where prdctid=@0", p);
            return Connection.Fetch<int>(qryTxt);
        }
        #endregion

        #region Product Tag Map
        /// <summary>
        /// Product tag mapping
        /// </summary>
        /// <returns></returns>
        public int PTMap(int productid, string[] tags)
        {
            int RetVal = 0;
            try
            {
                string qryTxt = "delete from producttagmapping where prdctid=" + productid + "";
                qryTxt += " INSERT INTO [dbo].[producttagmapping]([prdctid],[tagid])VALUES";
                foreach (string s in tags)
                {
                    qryTxt += " (" + productid + "," + Int32.Parse(s) + " ),";
                }
                Sql qry = Sql.Builder.Append(qryTxt.TrimEnd(','));
                RetVal = Connection.Execute(qry);
                this.Msg = "Record Saved Successfully !!!";
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                Msg = ex.Message;
            }
            return RetVal;
        }
        #endregion

        #region Product Category Map
        /// <summary>
        /// Product category mapping
        /// </summary>
        /// <returns></returns>
        public int PCMap(productcatmapping model)
        {
            int RetVal = 0;
            try
            {
                Sql qryTxt = Sql.Builder.Append("delete from productcatmapping where prdctid=" + model.prdctid);
                Connection.Execute(qryTxt);
                RetVal = Convert.ToInt32(Connection.Insert("productcatmapping", "pcmapid", model));
                this.Msg = "Record Saved Successfully !!!";
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                Msg = ex.Message;
            }
            return RetVal;
        }

        #endregion



    }
}