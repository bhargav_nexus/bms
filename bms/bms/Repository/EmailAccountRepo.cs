﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using bms.Models;
using PetaPoco;

namespace bms.Repository
{
    public class EmailAccountRepo : BaseConnection
    {
        public string GetSetError { get; set; }
        public emailaccountModel getaccount(Int64 acid)
        {
            emailaccountModel account = new emailaccountModel();
            Sql query = Sql.Builder.Append("select * from emailaccountMaster where eaid=@0", acid);
            account = Connection.SingleOrDefault<emailaccountModel>(query);
            return account;
        }
    }
}