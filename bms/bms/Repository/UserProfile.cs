﻿using bms.Models;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace bms.Repository
{
    public class UserProfile : BaseConnection
    {
        public bool ISSUCCESS { get; set; }
        public string MESSAGE { get; set; }

        public int Insert(UserProfile_Prop _prp)
        {
            int RetVal = 0;
            try
            {
                RetVal = Convert.ToInt32(Connection.Insert("UserProfile", "upid", _prp));
                this.MESSAGE = "Record Saved Successfully !!!";
                ISSUCCESS = true;
                return RetVal;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return RetVal;
            }
        }

        public int Update(UserProfile_Prop _prp)
        {
            int RetVal = 0;
            try
            {
                RetVal = Convert.ToInt32(Connection.Update("UserProfile", "upid", _prp, _prp.upid));
                this.MESSAGE = "Record Updated Successfully !!!";
                ISSUCCESS = true;
                return RetVal;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return RetVal;
            }
        }

        public List<UserProfile_Prop> GetAllRecords(String role, int page, out long ttlRec, int pagesize, string uname, string name)
        {
            ttlRec = 0;
            List<UserProfile_Prop> ItemList = new List<UserProfile_Prop>();
            try
            {
                Sql qryText = Sql.Builder.Append("select UserProfile.*,Users.UserName,Memberships.IsApproved as Status,Memberships.CreateDate,Memberships.LastLoginDate from UserProfile");
                qryText.Append("inner join Users on UserProfile.upuserid=Users.UserId");
                qryText.Append("inner join Memberships on UserProfile.upuserid=Memberships.UserId");
                qryText.Append("inner join UsersInRoles on UserProfile.upuserid=UsersInRoles.UserId");
                qryText.Append("inner join Roles on UsersInRoles.RoleId=Roles.RoleId");
                qryText.Append("where Roles.RoleName='" + role + "'");
                if (!String.IsNullOrEmpty(uname))
                {
                    qryText.Append("and UserName like'%" + uname + "%'");
                }
                if (!String.IsNullOrEmpty(name))
                {
                    qryText.Append("and upfname +' '+ uplname like '%" + name + "%'");
                }
                qryText.Append("order by UserProfile.upid desc");
                var data = Connection.Page<UserProfile_Prop>(page, pagesize, qryText);
                ItemList = data.Items.ToList<UserProfile_Prop>();
                ttlRec = data.TotalItems;
                ISSUCCESS = true;
                return ItemList;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return ItemList;
            }
        }

        //Get record by Primary Key
        public UserProfile_Prop GetRecordById(int PK)
        {
            UserProfile_Prop itemProp = new UserProfile_Prop();
            try
            {
                Sql qryText = Sql.Builder.Append("SELECT * FROM UserProfile WHERE upid = @0", PK);
                //Append For Long Query, Remove comment from below line
                //qryText.Append("");
                itemProp = Connection.Single<UserProfile_Prop>(qryText);
                ISSUCCESS = true;
                return itemProp;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return itemProp;
            }
        }

        //Get record by Primary Key
        public UserProfile_Prop GetRecordByUniqueId(Guid userid)
        {
            UserProfile_Prop itemProp = new UserProfile_Prop();
            try
            {
                Sql qryText = Sql.Builder.Append("SELECT UserProfile.*,Users.UserName FROM UserProfile inner join Users on UserProfile.upuserid=Users.Userid where UserProfile.upuserid = @0", userid);
                itemProp = Connection.Single<UserProfile_Prop>(qryText);
                ISSUCCESS = true;
                return itemProp;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return itemProp;
            }
        }



        //Delete record by Primary Key
        public int Delete(int PK)
        {
            int RetVal = 0;
            try
            {
                RetVal = Connection.Delete<UserProfile_Prop>("WHERE upid = @0", PK);
                this.MESSAGE = "Record Deleted Successfully !!!";
                ISSUCCESS = true;
                return RetVal;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return RetVal;
            }
        }
        public void ChangeStatus(Guid id)
        {
            MembershipUser usr = Membership.GetUser(id);
            usr.IsApproved = !usr.IsApproved;
            Membership.UpdateUser(usr);
        }
    }
}