﻿using bms.Models;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bms.Repository
{
    public class UserAddress : BaseConnection
    {
        public bool ISSUCCESS { get; set; }
        public string MESSAGE { get; set; }

        public int Insert(UserAddress_Prop _prp)
        {
            Sql qryText = Sql.Builder.Append("update UserAddress set uastatus=0 where uauserid=@0", _prp.uauserid);
            Connection.Execute(qryText);
            int RetVal = 0;
            try
            {
                RetVal = Convert.ToInt32(Connection.Insert("UserAddress", "uaid", _prp));
                this.MESSAGE = "Record Saved Successfully !!!";
                ISSUCCESS = true;
                return RetVal;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return RetVal;
            }
        }

        public int Update(UserAddress_Prop _prp)
        {
            int RetVal = 0;
            try
            {
                RetVal = Convert.ToInt32(Connection.Update("UserAddress", "uaid", _prp, _prp.uaid));
                this.MESSAGE = "Record Updated Successfully !!!";
                ISSUCCESS = true;
                return RetVal;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return RetVal;
            }
        }

        public List<UserAddress_Prop> GetAllRecords()
        {
            List<UserAddress_Prop> ItemList = new List<UserAddress_Prop>();
            try
            {
                Sql qryText = Sql.Builder.Append("SELECT * FROM UserAddress");
                qryText.Append("");
                ItemList = Connection.Fetch<UserAddress_Prop>(qryText);
                ISSUCCESS = true;
                return ItemList;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return ItemList;
            }
        }

        //Get record by Primary Key
        public UserAddress_Prop GetRecordById(int PK)
        {
            UserAddress_Prop itemProp = new UserAddress_Prop();
            try
            {
                Sql qryText = Sql.Builder.Append("SELECT * FROM UserAddress WHERE uaid = @0", PK);
                //qryText.Append("");
                itemProp = Connection.Single<UserAddress_Prop>(qryText);
                ISSUCCESS = true;
                return itemProp;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return itemProp;
            }
        }

        //Get record by Uniqueid
        public UserAddress_Prop GetRecordByUniqueId(Guid userid)
        {
            UserAddress_Prop itemProp = new UserAddress_Prop();
            try
            {
                Sql qryText = Sql.Builder.Append("select * from UserAddress where uauserid=@0 and uastatus=1", userid);
                itemProp = Connection.Single<UserAddress_Prop>(qryText);
                ISSUCCESS = true;
                return itemProp;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return itemProp;
            }
        }

        //Get BillingAddress by Uniqueid
        public UserAddress_Prop GetBillingAddressUniqueId(Guid userid)
        {
            UserAddress_Prop itemProp = new UserAddress_Prop();
            try
            {
                Sql qryText = Sql.Builder.Append("select * from UserAddress where uauserid=@0 and uastatus=1 and uatype=1", userid);
                itemProp = Connection.Single<UserAddress_Prop>(qryText);
                ISSUCCESS = true;
                return itemProp;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return itemProp;
            }
        }

        //Get ShippingAddress by Uniqueid
        public UserAddress_Prop GetShippingbyUniqueId(Guid userid)
        {
            UserAddress_Prop itemProp = new UserAddress_Prop();
            try
            {
                Sql qryText = Sql.Builder.Append("select * from UserAddress where uauserid=@0 and uastatus=1 and uatype=2", userid);
                itemProp = Connection.Single<UserAddress_Prop>(qryText);
                ISSUCCESS = true;
                return itemProp;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return itemProp;
            }
        }

        //Delete record by Primary Key
        public int Delete(int PK)
        {
            int RetVal = 0;
            try
            {
                RetVal = Connection.Delete<UserAddress_Prop>("WHERE uaid = @0", PK);
                this.MESSAGE = "Record Deleted Successfully !!!";
                ISSUCCESS = true;
                return RetVal;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return RetVal;
            }
        }
    }
}