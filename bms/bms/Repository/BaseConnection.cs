﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.ComponentModel;
using System.Web.Security;
using System.IO;

namespace bms.Repository
{
    public class BaseConnection
    {
        
        
        private static Database _Connection;
        public static Database Connection
        {
            get
            {
                if (_Connection == null)
                {
                    _Connection = new Database("DefaultConnection");
                }
                return _Connection;
            }
        }
    }
}