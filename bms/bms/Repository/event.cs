﻿using bms.Models;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bms.Repository
{
    public class eventRepo : BaseConnection
    {
          public bool ISSUCCESS{ get; set; }
        public string MESSAGE { get; set; }
            
        public int Insert(event_Prop _prp)
        {
            int RetVal = 0;
            try
            {
                RetVal = Convert.ToInt32(Connection.Insert("event", "eid", _prp));
                this.MESSAGE = "Record Saved Successfully !!!";
                ISSUCCESS = true;
                return RetVal;   
            }
            catch (Exception ex)
            {                
                MESSAGE = ex.Message;
                return RetVal;
            }
        }

        public int Update(event_Prop _prp)
        {
            int RetVal = 0;
            try
            {
                RetVal = Convert.ToInt32(Connection.Update("event", "eid", _prp, _prp.eid));
                this.MESSAGE = "Record Updated Successfully !!!";
                ISSUCCESS = true;
                return RetVal;   
            }
            catch (Exception ex)
            {                
                MESSAGE = ex.Message;
                return RetVal;
            }
        }

        public List<event_Prop> GetAllRecords()
        {
            List<event_Prop> ItemList = new List<event_Prop>();
            try
            {
                Sql qryText = Sql.Builder.Append("SELECT * FROM event");
                //Append For Long Query, Remove comment from below line
                //qryText.Append("");
                ItemList = Connection.Fetch<event_Prop>(qryText);
                ISSUCCESS = true;
                return ItemList;
            }
            catch (Exception ex)
            {                
                MESSAGE = ex.Message;
                return ItemList;
            }
        }
        
        //Get record by Primary Key
        public event_Prop GetRecordById(int PK)
        {
            event_Prop itemProp = new event_Prop();
            try
            {
               Sql qryText = Sql.Builder.Append("SELECT * FROM event WHERE eid = @0",PK);
                //Append For Long Query, Remove comment from below line
                //qryText.Append("");
                itemProp = Connection.Single<event_Prop>(qryText);
                ISSUCCESS = true;
                return itemProp;
            }
            catch (Exception ex)
            {                
                MESSAGE = ex.Message;
                return itemProp;
            }
        }
        
        //Delete record by Primary Key
        public int Delete(int PK)
        {
            int RetVal = 0;
            try
            {
                RetVal = Connection.Delete<event_Prop>("WHERE eid = @0",PK);
                this.MESSAGE = "Record Deleted Successfully !!!";
                ISSUCCESS = true;
                return RetVal;
            }
            catch (Exception ex)
            {                                
                MESSAGE = ex.Message;
                return RetVal;
            }            
        }

        public bool ChangeStatus(Int64 PK)
        {
            bool RetVal = false;
            try
            {
                Sql qryText = Sql.Builder.Append("SELECT * FROM event WHERE eid = @0", PK);
                event_Prop itemProp = Connection.Single<event_Prop>(qryText);
                itemProp.status = ! itemProp.status;
                Connection.Update(itemProp);
                this.MESSAGE = "Status Changed Successfully !!!";
                ISSUCCESS = true;
                RetVal = true;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
            }
            return RetVal;
        }
    }
}