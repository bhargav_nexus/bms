﻿using bms.Models;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bms.Repository
{
    public class sportperson : BaseConnection
    {
         public bool ISSUCCESS{ get; set; }
        public string MESSAGE { get; set; }
            
        public int Insert(sportperson_Prop _prp)
        {
            int RetVal = 0;
            try
            {
                RetVal = Convert.ToInt32(Connection.Insert("sportperson", "sid", _prp));
                this.MESSAGE = "Record Saved Successfully !!!";
                ISSUCCESS = true;
                return RetVal;   
            }
            catch (Exception ex)
            {                
                MESSAGE = ex.Message;
                return RetVal;
            }
        }

        public int Update(sportperson_Prop _prp)
        {
            int RetVal = 0;
            try
            {
                RetVal = Convert.ToInt32(Connection.Update("sportperson", "sid", _prp, _prp.sid));
                this.MESSAGE = "Record Updated Successfully !!!";
                ISSUCCESS = true;
                return RetVal;   
            }
            catch (Exception ex)
            {                
                MESSAGE = ex.Message;
                return RetVal;
            }
        }

        public List<sportperson_Prop> GetAllRecords()
        {
            List<sportperson_Prop> ItemList = new List<sportperson_Prop>();
            try
            {
                Sql qryText = Sql.Builder.Append("SELECT * FROM sportperson");
                //Append For Long Query, Remove comment from below line
                //qryText.Append("");
                ItemList = Connection.Fetch<sportperson_Prop>(qryText);
                ISSUCCESS = true;
                return ItemList;
            }
            catch (Exception ex)
            {                
                MESSAGE = ex.Message;
                return ItemList;
            }
        }
        
        //Get record by Primary Key
        public sportperson_Prop GetRecordById(int PK)
        {
            sportperson_Prop itemProp = new sportperson_Prop();
            try
            {
               Sql qryText = Sql.Builder.Append("SELECT * FROM sportperson WHERE sid = @0",PK);
                //Append For Long Query, Remove comment from below line
                //qryText.Append("");
                itemProp = Connection.Single<sportperson_Prop>(qryText);
                ISSUCCESS = true;
                return itemProp;
            }
            catch (Exception ex)
            {                
                MESSAGE = ex.Message;
                return itemProp;
            }
        }
        
        //Delete record by Primary Key
        public int Delete(int PK)
        {
            int RetVal = 0;
            try
            {
                RetVal = Connection.Delete<sportperson_Prop>("WHERE sid = @0",PK);
                this.MESSAGE = "Record Deleted Successfully !!!";
                ISSUCCESS = true;
                return RetVal;
            }
            catch (Exception ex)
            {                                
                MESSAGE = ex.Message;
                return RetVal;
            }            
        }
        public bool ChangeStatus(Int64 PK)
        {
            bool RetVal = false;
            try
            {
                Sql qryText = Sql.Builder.Append("SELECT * FROM sportperson WHERE sid = @0", PK);
                sportperson_Prop itemProp = Connection.Single<sportperson_Prop>(qryText);
                itemProp.status = !itemProp.status;
                Connection.Update(itemProp);
                this.MESSAGE = "Status Changed Successfully !!!";
                ISSUCCESS = true;
                RetVal = true;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
            }
            return RetVal;
        }
    }
}