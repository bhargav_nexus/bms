﻿using bms.Models;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bms.Repository
{
    public class category :BaseConnection
    {
        public bool ISSUCCESS { get; set; }
        public string MESSAGE { get; set; }

        public int Insert(category_Prop _prp)
        {
            int RetVal = 0;
            try
            {
                RetVal = Convert.ToInt32(Connection.Insert("category", "catid", _prp));
                this.MESSAGE = "Record Saved Successfully !!!";
                ISSUCCESS = true;
                return RetVal;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return RetVal;
            }
        }

        public int Update(category_Prop _prp)
        {
            int RetVal = 0;
            try
            {
                RetVal = Convert.ToInt32(Connection.Update("category", "catid", _prp, _prp.catid));
                this.MESSAGE = "Record Updated Successfully !!!";
                ISSUCCESS = true;
                return RetVal;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return RetVal;
            }
        }

        public List<category_Prop> GetAllRecords()
        {
            List<category_Prop> ItemList = new List<category_Prop>();
            try
            {
                Sql qryText = Sql.Builder.Append("SELECT * FROM category");
                //Append For Long Query, Remove comment from below line
                //qryText.Append("");
                ItemList = Connection.Fetch<category_Prop>(qryText);
                ISSUCCESS = true;
                return ItemList;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return ItemList;
            }
        }

        //Get record by Primary Key
        public category_Prop GetRecordById(int PK)
        {
            category_Prop itemProp = new category_Prop();
            try
            {
                Sql qryText = Sql.Builder.Append("SELECT * FROM category WHERE catid = @0", PK);
                //Append For Long Query, Remove comment from below line
                //qryText.Append("");
                itemProp = Connection.Single<category_Prop>(qryText);
                ISSUCCESS = true;
                return itemProp;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return itemProp;
            }
        }

        //Delete record by Primary Key
        public int Delete(int PK)
        {
            int RetVal = 0;
            try
            {
                RetVal = Connection.Delete<category_Prop>("WHERE catid = @0", PK);
                this.MESSAGE = "Record Deleted Successfully !!!";
                ISSUCCESS = true;
                return RetVal;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return RetVal;
            }
        }

        public bool ChangeStatus(Int64 PK)
        {
            bool RetVal = false;
            try
            {
                Sql qryText = Sql.Builder.Append("SELECT * FROM category WHERE catid = @0", PK);
                category_Prop itemProp = Connection.Single<category_Prop>(qryText);
                itemProp.status = !itemProp.status;
                Connection.Update(itemProp);
                this.MESSAGE = "Status Changed Successfully !!!";
                ISSUCCESS = true;
                RetVal = true;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
            }
            return RetVal;
        }
    }
}