﻿using bms.Models;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bms.Repository
{
    public class imageresources : BaseConnection
    {
        public bool ISSUCCESS { get; set; }
        public string MESSAGE { get; set; }

        public int Insert(imageresources_Prop _prp)
        {
            int RetVal = 0;
            try
            {
                RetVal = Convert.ToInt32(Connection.Insert("imageresources", "id", _prp));
                this.MESSAGE = "Record Saved Successfully !!!";
                ISSUCCESS = true;
                return RetVal;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return RetVal;
            }
        }

        public int Update(imageresources_Prop _prp)
        {
            int RetVal = 0;
            try
            {
                RetVal = Convert.ToInt32(Connection.Update("imageresources", "id", _prp, _prp.id));
                this.MESSAGE = "Record Updated Successfully !!!";
                ISSUCCESS = true;
                return RetVal;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return RetVal;
            }
        }

        public List<imageresources_Prop> GetAllRecords()
        {
            List<imageresources_Prop> ItemList = new List<imageresources_Prop>();
            try
            {
                Sql qryText = Sql.Builder.Append("SELECT * FROM imageresources");
                //Append For Long Query, Remove comment from below line
                //qryText.Append("");
                ItemList = Connection.Fetch<imageresources_Prop>(qryText);
                ISSUCCESS = true;
                return ItemList;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return ItemList;
            }
        }

        public List<imageresources_Prop> GetAllRecordsByres(Int64 resid)
        {
            List<imageresources_Prop> ItemList = new List<imageresources_Prop>();
            try
            {
                Sql qryText = Sql.Builder.Append("select * from imageresources where imgid="+resid);
                ItemList = Connection.Fetch<imageresources_Prop>(qryText);
                ISSUCCESS = true;
                return ItemList;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return ItemList;
            }
        }

        //Get record by Primary Key
        public imageresources_Prop GetRecordById(int PK)
        {
            imageresources_Prop itemProp = new imageresources_Prop();
            try
            {
                Sql qryText = Sql.Builder.Append("SELECT * FROM imageresources WHERE id = @0", PK);
                //Append For Long Query, Remove comment from below line
                //qryText.Append("");
                itemProp = Connection.Single<imageresources_Prop>(qryText);
                ISSUCCESS = true;
                return itemProp;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return itemProp;
            }
        }

        //Delete record by Primary Key
        public int Delete(int PK)
        {
            int RetVal = 0;
            try
            {
                RetVal = Connection.Delete<imageresources_Prop>("WHERE id = @0", PK);
                this.MESSAGE = "Record Deleted Successfully !!!";
                ISSUCCESS = true;
                return RetVal;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return RetVal;
            }
        }

        public String GetImageName(int PK)
        {
            String RetVal = "";
            try
            {
                Sql qryText = Sql.Builder.Append("select imgname from imageresources where id=" + PK);
                RetVal = Connection.SingleOrDefault<String>(qryText);
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
            }
            return RetVal;
        }
    }
}