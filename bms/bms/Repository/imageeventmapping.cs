﻿using bms.Models;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bms.Repository
{
    public class imageeventmapping : BaseConnection
    {
        public bool ISSUCCESS { get; set; }
        public string MESSAGE { get; set; }

        public int Insert(imageeventmapping_Prop _prp)
        {
            int RetVal = 0;
            Sql qryText = Sql.Builder.Append("SELECT * FROM imageeventmapping WHERE ieimageid = @0", _prp.ieimageid);
            imageeventmapping_Prop itemProp = Connection.SingleOrDefault<imageeventmapping_Prop>(qryText);

            if (itemProp != null)
            {
                itemProp.ieeventid = _prp.ieeventid;
                RetVal = this.Update(itemProp);
            }
            else
            {
                try
                {
                    RetVal = Convert.ToInt32(Connection.Insert("imageeventmapping", "ieid", _prp));
                    this.MESSAGE = "Record Saved Successfully !!!";
                    ISSUCCESS = true;
                }
                catch (Exception ex)
                {
                    MESSAGE = ex.Message;
                }
            }
            return RetVal;
        }

        public int Update(imageeventmapping_Prop _prp)
        {
            int RetVal = 0;
            try
            {
                RetVal = Convert.ToInt32(Connection.Update("imageeventmapping", "ieid", _prp, _prp.ieid));
                this.MESSAGE = "Record Updated Successfully !!!";
                ISSUCCESS = true;
                return RetVal;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return RetVal;
            }
        }

        public List<imageeventmapping_Prop> GetAllRecords()
        {
            List<imageeventmapping_Prop> ItemList = new List<imageeventmapping_Prop>();
            try
            {
                Sql qryText = Sql.Builder.Append("SELECT * FROM imageeventmapping");
                //Append For Long Query, Remove comment from below line
                //qryText.Append("");
                ItemList = Connection.Fetch<imageeventmapping_Prop>(qryText);
                ISSUCCESS = true;
                return ItemList;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return ItemList;
            }
        }

        //Get record by Primary Key
        public imageeventmapping_Prop GetRecordById(int PK)
        {
            imageeventmapping_Prop itemProp = new imageeventmapping_Prop();
            try
            {
                Sql qryText = Sql.Builder.Append("SELECT * FROM imageeventmapping WHERE ieid = @0", PK);
                //Append For Long Query, Remove comment from below line
                //qryText.Append("");
                itemProp = Connection.Single<imageeventmapping_Prop>(qryText);
                ISSUCCESS = true;
                return itemProp;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return itemProp;
            }
        }

        //Delete record by Primary Key
        public int Delete(int PK)
        {
            int RetVal = 0;
            try
            {
                RetVal = Connection.Delete<imageeventmapping_Prop>("WHERE ieid = @0", PK);
                this.MESSAGE = "Record Deleted Successfully !!!";
                ISSUCCESS = true;
                return RetVal;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return RetVal;
            }
        }
    }
}