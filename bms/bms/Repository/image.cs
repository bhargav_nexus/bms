﻿using bms.Models;
using bms.ViewModels;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bms.Repository
{
    public class image : BaseConnection
    {
        public bool ISSUCCESS { get; set; }
        public string MESSAGE { get; set; }

        public int Insert(image_Prop _prp)
        {
            int RetVal = 0;
            try
            {
                RetVal = Convert.ToInt32(Connection.Insert("image", "imgid", _prp));
                this.MESSAGE = "Record Saved Successfully !!!";
                ISSUCCESS = true;
                return RetVal;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return RetVal;
            }
        }

        public int Update(image_Prop _prp)
        {
            int RetVal = 0;
            try
            {
                RetVal = Convert.ToInt32(Connection.Update("image", "imgid", _prp, _prp.imgid));
                this.MESSAGE = "Record Updated Successfully !!!";
                ISSUCCESS = true;
                return RetVal;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return RetVal;
            }
        }

        public List<imageView> GetAllRecords()
        {
            List<imageView> ItemList = new List<imageView>();
            try
            {
                Sql qryText = Sql.Builder.Append("select i.*,event.ename,places.pname,sportperson.sname  from image i");
                qryText.Append("inner join imageeventmapping ie on i.imgid = ie.ieimageid");
                qryText.Append("inner join event on ie.ieeventid = event.eid");
                qryText.Append("inner join imageplacemapping ip on i.imgid = ip.ipimageid");
                qryText.Append("inner join places on ip.ipplaceid=places.pid");
                qryText.Append("inner join imagesportpersonmappnig isp on i.imgid = isp.isimageid");
                qryText.Append("inner join sportperson on isp.issportpersonid = sportperson.sid");

                ItemList = Connection.Fetch<imageView>(qryText);
                ISSUCCESS = true;
                return ItemList;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return ItemList;
            }
        }

        public List<imageView> GetAllRecordsByUser(Guid Userid)
        {
            List<imageView> ItemList = new List<imageView>();
            try
            {
                Sql qryText = Sql.Builder.Append("select i.*,event.ename,places.pname,sportperson.sname  from image i");
                qryText.Append("left join imageeventmapping ie on i.imgid = ie.ieimageid");
                qryText.Append("left join event on ie.ieeventid = event.eid");
                qryText.Append("left join imageplacemapping ip on i.imgid = ip.ipimageid");
                qryText.Append("left join places on ip.ipplaceid=places.pid");
                qryText.Append("left join imagesportpersonmappnig isp on i.imgid = isp.isimageid");
                qryText.Append("left join sportperson on isp.issportpersonid = sportperson.sid");
                qryText.Append("where imguser=@0",Userid);
                ItemList = Connection.Fetch<imageView>(qryText);
                ISSUCCESS = true;
                return ItemList;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return ItemList;
            }
        }

        public List<imageView> GetPagedRecordsByUser(int page, out long ttlRec, int pagesize, Guid Userid)
        {
            ttlRec = 0;
            List<imageView> ItemList = new List<imageView>();
            try
            {
                Sql qryText = Sql.Builder.Append("select i.*,event.ename,places.pname,sportperson.sname  from image i");
                qryText.Append("left join imageeventmapping ie on i.imgid = ie.ieimageid");
                qryText.Append("left join event on ie.ieeventid = event.eid");
                qryText.Append("left join imageplacemapping ip on i.imgid = ip.ipimageid");
                qryText.Append("left join places on ip.ipplaceid=places.pid");
                qryText.Append("left join imagesportpersonmappnig isp on i.imgid = isp.isimageid");
                qryText.Append("left join sportperson on isp.issportpersonid = sportperson.sid");
                qryText.Append("where imguser=@0", Userid);
                var data = Connection.Page<imageView>(page, pagesize, qryText);
                ItemList = data.Items.ToList<imageView>();
                ttlRec = data.TotalItems;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
            }
            return ItemList;
        }

        //Get record by Primary Key
        public imageView GetRecordById(Int64 PK)
        {
            imageView itemProp = new imageView();
            try
            {
                Sql qryText = Sql.Builder.Append("select i.*,ISNUll(ie.ieeventid,0) as ename,ISNULL(ip.ipplaceid,0)as pname,ISNUll(isp.issportpersonid,0) as sname  from image i");
                qryText.Append("left join imageeventmapping ie on i.imgid = ie.ieimageid");
                qryText.Append("left join imageplacemapping ip on i.imgid = ip.ipimageid");
                qryText.Append("left join imagesportpersonmappnig isp on i.imgid = isp.isimageid");
                qryText.Append("WHERE imgid = @0", PK);
                itemProp = Connection.Single<imageView>(qryText);
                ISSUCCESS = true;
                return itemProp;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return itemProp;
            }
        }


        public image_Prop GetRecordByPrimary(Int64 PK)
        {
            image_Prop itemProp = new image_Prop();
            try
            {
                Sql qryText = Sql.Builder.Append("select * from image");
                qryText.Append("WHERE imgid = @0", PK);
                itemProp = Connection.Single<image_Prop>(qryText);
                ISSUCCESS = true;
                return itemProp;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return itemProp;
            }
        }
        //Delete record by Primary Key
        public int Delete(int PK)
        {
            int RetVal = 0;
            try
            {
                RetVal = Connection.Delete<image_Prop>("WHERE imgid = @0", PK);
                this.MESSAGE = "Record Deleted Successfully !!!";
                ISSUCCESS = true;
                return RetVal;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return RetVal;
            }
        }

        public bool ChangeStatus(Int64 PK)
        {
            bool RetVal = false;
            try
            {
                Sql qryText = Sql.Builder.Append("SELECT * FROM image WHERE imgid = @0", PK);
                image_Prop itemProp = Connection.Single<image_Prop>(qryText);
                itemProp.status = !itemProp.status;
                Connection.Update(itemProp);
                this.MESSAGE = "Status Changed Successfully !!!";
                ISSUCCESS = true;
                RetVal = true;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
            }
            return RetVal;
        }
    }
}