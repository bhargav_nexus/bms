﻿using bms.Models;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bms.Repository
{
    public class imagesportpersonmappnig : BaseConnection
    {
        public bool ISSUCCESS { get; set; }
        public string MESSAGE { get; set; }

        public int Insert(imagesportpersonmappnig_Prop _prp)
        {
            int RetVal = 0;
            Sql qryText = Sql.Builder.Append("SELECT * FROM imagesportpersonmappnig WHERE isimageid = @0", _prp.isimageid);
            imagesportpersonmappnig_Prop itemProp = Connection.SingleOrDefault<imagesportpersonmappnig_Prop>(qryText);

            if (itemProp != null)
            {
                itemProp.issportpersonid = _prp.issportpersonid;
                RetVal = this.Update(itemProp);
                this.MESSAGE = "Record Updated Successfully !!!";
            }
            else
            {
                try
                {
                    RetVal = Convert.ToInt32(Connection.Insert("imagesportpersonmappnig", "isid", _prp));
                    this.MESSAGE = "Record Saved Successfully !!!";
                    ISSUCCESS = true;
                }
                catch (Exception ex)
                {
                    MESSAGE = ex.Message;
                }
            }
            return RetVal;
        }

        public int Update(imagesportpersonmappnig_Prop _prp)
        {
            int RetVal = 0;
            try
            {
                RetVal = Convert.ToInt32(Connection.Update("imagesportpersonmappnig", "isid", _prp, _prp.isid));
                this.MESSAGE = "Record Updated Successfully !!!";
                ISSUCCESS = true;
                return RetVal;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return RetVal;
            }
        }

        public List<imagesportpersonmappnig_Prop> GetAllRecords()
        {
            List<imagesportpersonmappnig_Prop> ItemList = new List<imagesportpersonmappnig_Prop>();
            try
            {
                Sql qryText = Sql.Builder.Append("SELECT * FROM imagesportpersonmappnig");
                //Append For Long Query, Remove comment from below line
                //qryText.Append("");
                ItemList = Connection.Fetch<imagesportpersonmappnig_Prop>(qryText);
                ISSUCCESS = true;
                return ItemList;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return ItemList;
            }
        }

        //Get record by Primary Key
        public imagesportpersonmappnig_Prop GetRecordById(int PK)
        {
            imagesportpersonmappnig_Prop itemProp = new imagesportpersonmappnig_Prop();
            try
            {
                Sql qryText = Sql.Builder.Append("SELECT * FROM imagesportpersonmappnig WHERE isid = @0", PK);
                //Append For Long Query, Remove comment from below line
                //qryText.Append("");
                itemProp = Connection.Single<imagesportpersonmappnig_Prop>(qryText);
                ISSUCCESS = true;
                return itemProp;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return itemProp;
            }
        }

        //Delete record by Primary Key
        public int Delete(int PK)
        {
            int RetVal = 0;
            try
            {
                RetVal = Connection.Delete<imagesportpersonmappnig_Prop>("WHERE isid = @0", PK);
                this.MESSAGE = "Record Deleted Successfully !!!";
                ISSUCCESS = true;
                return RetVal;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return RetVal;
            }
        }
    }
}