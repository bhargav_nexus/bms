﻿
using bms.Models;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bms.Repository
{
    public class places : BaseConnection
    {
        public bool ISSUCCESS { get; set; }
        public string MESSAGE { get; set; }

        public int Insert(places_Prop _prp)
        {
            int RetVal = 0;
            try
            {
                RetVal = Convert.ToInt32(Connection.Insert("places", "pid", _prp));
                this.MESSAGE = "Record Saved Successfully !!!";
                ISSUCCESS = true;
                return RetVal;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return RetVal;
            }
        }

        public int Update(places_Prop _prp)
        {
            int RetVal = 0;
            try
            {
                RetVal = Convert.ToInt32(Connection.Update("places", "pid", _prp, _prp.pid));
                this.MESSAGE = "Record Updated Successfully !!!";
                ISSUCCESS = true;
                return RetVal;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return RetVal;
            }
        }

        public List<places_Prop> GetAllRecords()
        {
            List<places_Prop> ItemList = new List<places_Prop>();
            try
            {
                Sql qryText = Sql.Builder.Append("SELECT * FROM places");
                //Append For Long Query, Remove comment from below line
                //qryText.Append("");
                ItemList = Connection.Fetch<places_Prop>(qryText);
                ISSUCCESS = true;
                return ItemList;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return ItemList;
            }
        }

        //Get record by Primary Key
        public places_Prop GetRecordById(int PK)
        {
            places_Prop itemProp = new places_Prop();
            try
            {
                Sql qryText = Sql.Builder.Append("SELECT * FROM places WHERE pid = @0", PK);
                //Append For Long Query, Remove comment from below line
                //qryText.Append("");
                itemProp = Connection.Single<places_Prop>(qryText);
                ISSUCCESS = true;
                return itemProp;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return itemProp;
            }
        }

        //Delete record by Primary Key
        public int Delete(int PK)
        {
            int RetVal = 0;
            try
            {
                RetVal = Connection.Delete<places_Prop>("WHERE pid = @0", PK);
                this.MESSAGE = "Record Deleted Successfully !!!";
                ISSUCCESS = true;
                return RetVal;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return RetVal;
            }
        }

        public bool ChangeStatus(Int64 PK)
        {
            bool RetVal = false;
            try
            {
                Sql qryText = Sql.Builder.Append("SELECT * FROM places WHERE pid = @0", PK);
                places_Prop itemProp = Connection.Single<places_Prop>(qryText);
                itemProp.pstatus = !itemProp.pstatus;
                Connection.Update(itemProp);
                this.MESSAGE = "Status Changed Successfully !!!";
                ISSUCCESS = true;
                RetVal = true;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
            }
            return RetVal;
        }
    }
}