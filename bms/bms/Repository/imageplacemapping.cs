﻿using bms.Models;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bms.Repository
{
    public class imageplacemapping : BaseConnection
    {
        public bool ISSUCCESS { get; set; }
        public string MESSAGE { get; set; }

        public int Insert(imageplacemapping_Prop _prp)
        {
            int RetVal = 0;
            Sql qryText = Sql.Builder.Append("SELECT * FROM imageplacemapping WHERE ipimageid = @0", _prp.ipimageid);
            imageplacemapping_Prop itemProp = Connection.SingleOrDefault<imageplacemapping_Prop>(qryText);

            if (itemProp != null)
            {
                itemProp.ipplaceid = _prp.ipplaceid;
                RetVal = this.Update(itemProp);
                this.MESSAGE = "Record Updated Successfully !!!";
            }
            else
            {
                try
                {
                    RetVal = Convert.ToInt32(Connection.Insert("imageplacemapping", "ipid", _prp));
                    this.MESSAGE = "Record Saved Successfully !!!";
                    ISSUCCESS = true;
                }
                catch (Exception ex)
                {
                    MESSAGE = ex.Message;
                }
            }
            return RetVal;
        }

        public int Update(imageplacemapping_Prop _prp)
        {
            int RetVal = 0;
            try
            {
                RetVal = Convert.ToInt32(Connection.Update("imageplacemapping", "ipid", _prp, _prp.ipid));
                this.MESSAGE = "Record Updated Successfully !!!";
                ISSUCCESS = true;
                return RetVal;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return RetVal;
            }
        }

        public List<imageplacemapping_Prop> GetAllRecords()
        {
            List<imageplacemapping_Prop> ItemList = new List<imageplacemapping_Prop>();
            try
            {
                Sql qryText = Sql.Builder.Append("SELECT * FROM imageplacemapping");
                //Append For Long Query, Remove comment from below line
                //qryText.Append("");
                ItemList = Connection.Fetch<imageplacemapping_Prop>(qryText);
                ISSUCCESS = true;
                return ItemList;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return ItemList;
            }
        }

        //Get record by Primary Key
        public imageplacemapping_Prop GetRecordById(int PK)
        {
            imageplacemapping_Prop itemProp = new imageplacemapping_Prop();
            try
            {
                Sql qryText = Sql.Builder.Append("SELECT * FROM imageplacemapping WHERE ipid = @0", PK);
                //Append For Long Query, Remove comment from below line
                //qryText.Append("");
                itemProp = Connection.Single<imageplacemapping_Prop>(qryText);
                ISSUCCESS = true;
                return itemProp;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return itemProp;
            }
        }

        //Delete record by Primary Key
        public int Delete(int PK)
        {
            int RetVal = 0;
            try
            {
                RetVal = Connection.Delete<imageplacemapping_Prop>("WHERE ipid = @0", PK);
                this.MESSAGE = "Record Deleted Successfully !!!";
                ISSUCCESS = true;
                return RetVal;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return RetVal;
            }
        }
    }
}