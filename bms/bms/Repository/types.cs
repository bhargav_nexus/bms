﻿using bms.Models;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bms.Repository
{
    public class types : BaseConnection
    {
        public bool ISSUCCESS { get; set; }
        public string MESSAGE { get; set; }

        public int Insert(types_Prop _prp)
        {
            int RetVal = 0;
            try
            {
                RetVal = Convert.ToInt32(Connection.Insert("types", "tid", _prp));
                this.MESSAGE = "Record Saved Successfully !!!";
                ISSUCCESS = true;
                return RetVal;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return RetVal;
            }
        }

        public int Update(types_Prop _prp)
        {
            int RetVal = 0;
            try
            {
                RetVal = Convert.ToInt32(Connection.Update("types", "tid", _prp, _prp.tid));
                this.MESSAGE = "Record Updated Successfully !!!";
                ISSUCCESS = true;
                return RetVal;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return RetVal;
            }
        }

        public List<types_Prop> GetAllRecords()
        {
            List<types_Prop> ItemList = new List<types_Prop>();
            try
            {
                Sql qryText = Sql.Builder.Append("SELECT * FROM types");
                ItemList = Connection.Fetch<types_Prop>(qryText);
                ISSUCCESS = true;
                return ItemList;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return ItemList;
            }
        }

        //Get record by Primary Key
        public types_Prop GetRecordById(Int64 PK)
        {
            types_Prop itemProp = new types_Prop();
            try
            {
                Sql qryText = Sql.Builder.Append("SELECT * FROM types WHERE tid = @0", PK);
                itemProp = Connection.Single<types_Prop>(qryText);
                ISSUCCESS = true;
                return itemProp;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return itemProp;
            }
        }

        //Delete record by Primary Key
        public int Delete(int PK)
        {
            int RetVal = 0;
            try
            {
                RetVal = Connection.Delete<types_Prop>("WHERE tid = @0", PK);
                this.MESSAGE = "Record Deleted Successfully !!!";
                ISSUCCESS = true;
                return RetVal;
            }
            catch (Exception ex)
            {
                MESSAGE = ex.Message;
                return RetVal;
            }
        }

        public int gettypebyresolution(long height,long width)
        {
            int res = 0;
            Sql qryText = Sql.Builder.Append("select * from types where theight="+height+" and twidth="+width);
            res = Connection.SingleOrDefault<int>(qryText);
            return res;
        }
    }
}