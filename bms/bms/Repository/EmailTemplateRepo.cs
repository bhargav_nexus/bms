﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
using bms.Models;

namespace bms.Repository
{
    public class EmailTemplateRepo : BaseConnection
    {
        public string GetSetError { get; set; }
        public emailtemplateModel gettemplatebyname(String templatename)
        {
            emailtemplateModel template = new emailtemplateModel();
            Sql query = Sql.Builder.Append("select * from emailtemplateMaster where etname=@0", templatename);
            template = Connection.SingleOrDefault<emailtemplateModel>(query);
            return template;
        }

        public emailtemplateModel gettemplatebyid(Int64 id)
        {
            emailtemplateModel template = new emailtemplateModel();
            Sql query = Sql.Builder.Append("select * from emailtemplateMaster where etid=@0", id);
            template = Connection.SingleOrDefault<emailtemplateModel>(query);
            return template;
        }
        public List<emailtemplateModel> TemplateList()
        {
            List<emailtemplateModel> templates = new List<emailtemplateModel>();
            Sql qery = Sql.Builder.Append("Select * from emailtemplateMaster");
            templates = Connection.Fetch<emailtemplateModel>(qery);
            return templates;
        }

        public Boolean EditTemplate(emailtemplateModel mdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Update("emailtemplateMaster", "etid", mdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Updtae Template";
                blnRslt = false;
            }
            return blnRslt;
        }
    }
}