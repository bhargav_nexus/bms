﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using bms.Models;
using bms.Repository;
using bms.ViewModels;
using System.IO;
namespace bms.Controllers
{
    public class ProductController : Controller
    {
        products pmodel = new products();
        productsRepo prepo = new productsRepo();
        //
        // GET: /Product/

        #region Products
        public ActionResult Index()
        {
            List<ProductsViewModel> lst = new List<ProductsViewModel>();
            int page = 1;
            if (Request["pageno"] != null) { page = Convert.ToInt32(Request["pageno"]); }
            long ttlrec = 0;
            int pagesize = 10;
            int cateid = 0;
            string product = string.Empty;
            int tagid = 0;
            if (Session["CatLst"] == null)
            {
                Session["CatLst"] = prepo.GetCategory(false);
            }
            if (Session["Products"] == null)
            {
                Session["Products"] = prepo.GetAllProduct();
            }
            if (Session["PTags"] == null)
            {
                Session["PTags"] = prepo.GetTag();
            }
            if (!String.IsNullOrEmpty(Request["category"]))
            {
                cateid = Convert.ToInt32(Request["category"]);
            }
            if (!String.IsNullOrEmpty(Request["product"]))
            {
                product = Request["product"];
            }
            if (!String.IsNullOrEmpty(Request["tag"]))
            {
                tagid = Convert.ToInt32(Request["tag"]);
            }
            //string sort = "";
            //string sorttype = "";//asc or desc
            //if (Request["sort"] != null)
            //{
            //    lst = (List<ProductsViewModel>)Session["Products"];
            //    sort = Request["sort"];
            //    lst = lst.OrderByDescending(x => x.productname).ToList();
            //}
            //else
            //{
            lst = prepo.GetPagedProduct(page, out ttlrec, pagesize, cateid, tagid, product);
            //Session["Products"] = lst;
            //}
            ViewBag.tagid = tagid;
            ViewBag.cateid = cateid;
            ViewBag.product = product;
            ViewBag.pageNo = page;
            ViewBag.pageSize = pagesize;
            ViewBag.TotalRec = ttlrec;
            return View(lst);
        }
        public ActionResult AddProduct()
        {
            List<productcategory> category = new List<productcategory>();
            if (Session["CatLstActive"] == null)
            {
                category = prepo.GetCategory(true);
                Session["CatLstActive"] = category;
            }
            List<producttags> tags = new List<producttags>();
            if (Session["TagLst"] == null)
            {
                tags = prepo.GetTag();
                Session["TagLst"] = tags;
            }
            products prdct = new products();
            prdct.tagslst = new List<int>();
            if (Session["ProductById"] != null)
            {
                prdct = (products)Session["ProductById"];
                prdct.tagslst = prepo.GetTagsByPId(prdct.productid);
                Session["ProductById"] = null;
            }
            return View(prdct);
        }
        [HttpPost]
        public ActionResult AddProduct(products model)
        {
            //if (imagefile != null)
            //{
            //    model.prdctnewimgname = DateTime.Now.Ticks + "." + imagefile.ContentType.Substring(imagefile.ContentType.LastIndexOf('/') + 1);
            //    model.productimgname = imagefile.FileName;
            //    imagefile.SaveAs(HttpContext.Server.MapPath("~/ProductImages/" + model.prdctnewimgname));
            //}
            //else
            //{
            //    MySession.Current.MESSAGE = "Please upload product image";
            //    return View();
            //}
            if (String.IsNullOrEmpty(model.productname))
            {
                MySession.Current.MESSAGE = "Enter product name";
                return View();
            }
            int PrdctId = 0;
            if (model.productid > 0)
            {
                PrdctId = model.productid;
                int retval = prepo.UpdateProduct(model);
            }
            else
            {
                PrdctId = prepo.InsertProduct(model);
            }

            if (PrdctId > 0)
            {
                int RetTagMapid = 0; int RetCatMapId = 0;

                //prdct tag map
                if (Request["tags[]"] != null)
                {
                    string[] tags = Request["tags[]"].ToString().Split(',');
                    RetTagMapid = prepo.PTMap(PrdctId, tags);
                }
                if (Request["category"] != null)
                {
                    productcatmapping pc = new productcatmapping();
                    pc.prdctid = PrdctId;
                    pc.catid = Convert.ToInt32(Request["category"]);
                    RetCatMapId = prepo.PCMap(pc);
                }
                Session["Products"] = null;
                MySession.Current.MESSAGE = prepo.Msg;
                return RedirectToAction("Index");
            }
            else
            {
                MySession.Current.MESSAGE = prepo.Msg;
                return View();
            }
        }
        public ActionResult ProductImgs(string id)
        {
            ViewBag.PrdctId = id;
            List<productimages> prdcts = prepo.GetProductImgsById(Convert.ToInt32(id));
            ViewBag.Product = prepo.GetProductById(Convert.ToInt32(id)).productname.ToUpper();
            return View(prdcts);
        }
        public ActionResult DeleteProductImage()
        {
            int imgid = 0;
            if (Request["ImgId"] != null) { imgid = Convert.ToInt32(Request["Imgid"]); }
            productimages prdct = prepo.GetProductImgById(Convert.ToInt32(imgid));
            int retid = prepo.DeleteProductImg(imgid);
            String StorageRoot = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/ProductImages/"));
            var filePath = StorageRoot + prdct.productimgname;
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }
            MySession.Current.MESSAGE = "Prduct image deleted successfully";
            return RedirectToAction("ProductImgs", new { id = prdct.productid.ToString() });
        }
        public ActionResult ViewProduct(string id)
        {
            Session["ProductById"] = prepo.GetProductById(Convert.ToInt32(id));
            return RedirectToAction("AddProduct");
        }
        public JsonResult DltProduct(string id)
        {
            int Ret = 0;
            Ret = prepo.DeleteProduct(Convert.ToInt32(id));
            if (Ret == 0)
            {
                MySession.Current.MESSAGE = "Product cannot be delete !!!";
            }
            else
            {
                Session["Products"] = null;
            }
            return Json(Ret);
        }

        #endregion

        #region Category
        public ActionResult Category()
        {
            List<productcategory> lst = new List<productcategory>();
            if (Session["CatLst"] == null)
            {
                lst = prepo.GetCategory(false);
                Session["CatLst"] = lst;
            }
            else
            {
                lst = (List<productcategory>)Session["CatLst"];
            }
            return View(lst);
        }
        [HttpPost]
        public ActionResult AddCategory(productcategory model)
        {
            int Retval = 0;
            if (!String.IsNullOrEmpty(model.productcatname))
            {
                if (Request["catstatus"] == "1") model.catstatus = true;
                Retval = prepo.AddCategory(model);
                if (Retval > 0)
                {
                    Session["CatLst"] = null;
                }

                MySession.Current.MESSAGE = prepo.Msg;
            }
            return RedirectToAction("Category");
        }
        public JsonResult ChangeCatSts(string id)
        {
            int ret = 0;
            if (!String.IsNullOrEmpty(id))
            {
                ret = prepo.ChangeCatStatus(Convert.ToInt32(id));
                if (ret > 0) { Session["CatLst"] = Session["CatLstActive"] = null; }
            }
            return Json(ret);
        }
        #endregion

        #region Tags
        public ActionResult Tag()
        {
            List<producttags> lst = new List<producttags>();
            if (Session["TagLst"] == null)
            {
                lst = prepo.GetTag();
                Session["TagLst"] = lst;
            }
            else
            {
                lst = (List<producttags>)Session["TagLst"];
            }
            return View(lst);
        }
        [HttpPost]
        public ActionResult AddTag(producttags model)
        {
            int Retval = 0;
            if (!String.IsNullOrEmpty(model.tagname))
            {
                Retval = prepo.AddTag(model);
                if (Retval > 0)
                {
                    Session["PTags"] = Session["TagLst"] = null;

                }
                MySession.Current.MESSAGE = prepo.Msg;
            }
            return RedirectToAction("Tag");
        }
        public JsonResult DltCat(string id)
        {
            int Ret = 0;
            Ret = prepo.DeleteTag(Convert.ToInt32(id));
            if (Ret == 0)
            {
                MySession.Current.MESSAGE = "Tag cannot be delete !!!";
            }
            else
            {
                Session["TagLst"] = null;
            }
            return Json(Ret);
        }
        #endregion
    }
}
