﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using bms.Repository;
using bms.Models;
using bms.ViewModels;
using System.Web.Security;
using System.Net.Http;
using System.IO;
using System.Drawing;
using LevDan.Exif;
using System.Text;
using Microsoft.WindowsAPICodePack.Shell.PropertySystem;
using Microsoft.WindowsAPICodePack.Shell;
using System.Diagnostics;


namespace bms.Controllers
{
    public class ImageInventoryController : Controller
    {
        int Page = 1;
        long TotalRecs = 0;
        int PageSize = 10;
        string imagename, events, sportperson, tag;

        //
        // GET: /ImageInventory/
        image imagerepoo = new image();
        imageresources imageresourcerepo = new imageresources();
        imageeventmapping eventrepo = new imageeventmapping();
        imageplacemapping placerepo = new imageplacemapping();
        imagesportpersonmappnig sportpersonrepo = new imagesportpersonmappnig();
        imagetagmapping tagrepo = new imagetagmapping();
        imageyearmapping yearrepo = new imageyearmapping();


        #region
        public JsonResult GetAll()
        {
            MembershipUser muser = Membership.GetUser();
            List<imageView> images = imagerepoo.GetAllRecordsByUser((Guid)muser.ProviderUserKey);
            return Json(images, JsonRequestBehavior.AllowGet);
        }
        #endregion


        [Authorize(Roles = "Provider")]
        public ActionResult Index()
        {
            String qurystring = string.Empty;
            if (!String.IsNullOrEmpty(Request["pageno"])) { Page = Convert.ToInt32(Request["pageno"]); }

            if (!String.IsNullOrEmpty(Request["imagename"]))
            {
                imagename = Request["pageno"].ToString();
                @ViewBag.IMAGENAME = imagename;
            }

            if (!String.IsNullOrEmpty(Request["imagename"])) { imagename = Request["pageno"].ToString(); }

            MembershipUser muser = Membership.GetUser();
            List<imageView> images = imagerepoo.GetPagedRecordsByUser(Page, out TotalRecs, PageSize, (Guid)muser.ProviderUserKey);
            ViewBag.pageNo = Page;
            ViewBag.pageSize = PageSize;
            ViewBag.TotalRec = TotalRecs;

            return View(images);
        }

        [Authorize(Roles = "Provider")]
        public ActionResult AddNewImage()
        {
            return View();
        }

        [Authorize(Roles = "Provider")]
        [HttpPost]
        public ActionResult AddNewImage(image_Prop prop, String Event, String SportPerson, String Tags, String Places)
        {
            prop.imginsertdate = DateTime.Now;
            prop.status = true;
            prop.imguser = (Guid)Membership.GetUser().ProviderUserKey;
            int i = imagerepoo.Insert(prop);
            if (i > 0)
            {
                if (Event != null)
                {
                    // event mapping
                    imageeventmapping_Prop model = new imageeventmapping_Prop();
                    model.ieimageid = i;
                    model.ieeventid = Convert.ToInt64(Event);
                    eventrepo.Insert(model);
                }
                if (SportPerson != null)
                {
                    // sportperson mapping
                    imagesportpersonmappnig_Prop model = new imagesportpersonmappnig_Prop();
                    model.isimageid = i;
                    model.issportpersonid = Convert.ToInt64(SportPerson);
                    sportpersonrepo.Insert(model);
                }
                if (Tags != null)
                {
                    // tag mapping
                    String[] tgs = Tags.Split(',');
                    foreach (String t in tgs)
                    {
                        imagetagmapping_Prop model = new imagetagmapping_Prop();
                        model.itimageid = i;
                        model.ittagid = Convert.ToInt64(t);
                        tagrepo.Insert(model);
                    }
                }
                if (Places != null)
                {
                    // place mapping
                    imageplacemapping_Prop model = new imageplacemapping_Prop();
                    model.ipimageid = i;
                    model.ipplaceid = Convert.ToInt64(Places);
                    placerepo.Insert(model);
                }

                MySession.Current.MESSAGE = "Image Uploded Successfully";
                return RedirectToAction("Index", "ImageInventory");
            }
            else
            {
                MySession.Current.MESSAGE = imagerepoo.MESSAGE;
                return View();
            }
        }

        [Authorize(Roles = "Provider")]
        public ActionResult EditImage(Int64 id)
        {
            imageView image = imagerepoo.GetRecordById(id);
            return View(image);
        }

        [Authorize(Roles = "Provider")]
        [HttpPost]
        public ActionResult EditImage(image_Prop prop, String Event, String SportPerson, String Tags, String Places)
        {
            prop.imguser = (Guid)Membership.GetUser().ProviderUserKey;
            int i = imagerepoo.Update(prop);
            if (i > 0)
            {
                if (Event != null)
                {
                    // event mapping
                    imageeventmapping_Prop model = new imageeventmapping_Prop();
                    model.ieimageid = i;
                    model.ieeventid = Convert.ToInt64(Event);
                    eventrepo.Insert(model);
                }
                if (SportPerson != null)
                {
                    // sportperson mapping
                    imagesportpersonmappnig_Prop model = new imagesportpersonmappnig_Prop();
                    model.isimageid = i;
                    model.issportpersonid = Convert.ToInt64(SportPerson);
                    sportpersonrepo.Insert(model);
                }
                if (Tags != null && Tags != "")
                {
                    // tag mapping
                    String[] tgs = Tags.Split(',');
                    foreach (String t in tgs)
                    {
                        imagetagmapping_Prop model = new imagetagmapping_Prop();
                        model.itimageid = i;
                        model.ittagid = Convert.ToInt64(t);
                        tagrepo.Insert(model);
                    }
                }
                if (Places != null)
                {
                    // place mapping
                    imageplacemapping_Prop model = new imageplacemapping_Prop();
                    model.ipimageid = i;
                    model.ipplaceid = Convert.ToInt64(Places);
                    placerepo.Insert(model);
                }

                MySession.Current.MESSAGE = "Image Uploded Successfully";
                return RedirectToAction("Index", "ImageInventory");
            }
            else
            {
                MySession.Current.MESSAGE = imagerepoo.MESSAGE;
                return View();
            }
        }

        public ActionResult ChangeStatus(Int64 id)
        {
            imagerepoo.ChangeStatus(id);
            MySession.Current.MESSAGE = imagerepoo.MESSAGE;
            return RedirectToAction("Index", "ImageInventory");
        }

        public JsonResult Uploadfile(object fileUploadObj)
        {
            HttpPostedFileBase file = Request.Files[0];
            //file.SaveAs(HttpContext.Server.MapPath("Images"));
            return Json("Success");
        }

        #region

        public ActionResult ManageResources(Int64 id)
        {
            image_Prop image = imagerepoo.GetRecordByPrimary(id);
            ViewBag.IMAGENAME = image.imgname;
            ViewBag.IMAGEID = id;
            List<imageresources_Prop> resources = imageresourcerepo.GetAllRecordsByres(id);
            ViewBag.RESOURCES = resources;
            return View();
        }

        public ActionResult DeleteResources(int id, Int64 resid)
        {
            String fname = imageresourcerepo.GetImageName(id);
            imageresourcerepo.Delete(id);

            String StorageRoot = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/Images/"));
            var filePath = StorageRoot + fname;
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }
            return RedirectToAction("ManageResources", "ImageInventory", new { id = resid });
        }
        #endregion

        #region
        public ActionResult Test()
        {
            imagedata i = new imagedata();
            return View(i);
        }

        [HttpPost]
        public ActionResult Test(HttpPostedFileBase imagefile)
        {
            String rootpath = Server.MapPath("~/TestImage/");
            String fullpath = string.Empty;
            if (imagefile != null)
            {
                fullpath = rootpath + imagefile.FileName;
                imagefile.SaveAs(fullpath);
            }
           imagedata id = readimage(fullpath);
            return View(id);
        }

        private imagedata readimage(String filename)
        {
            Image img = new Bitmap(filename);
            var propitems = img.PropertyItems;

            String auth = string.Empty;

            ExifTagCollection exif = new ExifTagCollection(filename);

            foreach (ExifTag tag in exif)
            {
                if (tag.FieldName == "Artist")
                {
                    auth = tag.Value;
                }
            }

            ShellObject picture = ShellObject.FromParsingName(filename);
            var datetaken = picture.Properties.GetProperty(SystemProperties.System.Photo.DateTaken).ValueAsObject;
            var camera = picture.Properties.GetProperty(SystemProperties.System.Photo.CameraManufacturer).ValueAsObject;
            var cameraModel = picture.Properties.GetProperty(SystemProperties.System.Photo.CameraModel).ValueAsObject;
            String[]  tags = (String[])picture.Properties.GetProperty(SystemProperties.System.Photo.TagViewAggregate).ValueAsObject;
            var events = picture.Properties.GetProperty(SystemProperties.System.Photo.Event).ValueAsObject;
            
            String temp = string.Empty;
            foreach (String s in tags)
            {
                temp = temp + s + ",";
            }
            imagedata id = new imagedata();
            id.imagename = picture.Name;
            id.datetaken = datetaken.ToString();
            id.height = img.Height.ToString();
            id.width = img.Width.ToString();
            id.camera = camera.ToString();
            id.cameraModel = cameraModel.ToString();
            id.tags = temp.Trim(',');
            id.author = auth;
            return id;
        }
        #endregion
    }

    public class imagedata
    {
        public string imagename { get; set; }
        public string height { get; set; }
        public string width { get; set; }
        public string datetaken { get; set; }
        public string tags { get; set; }
        public string camera { get; set; }
        public string cameraModel { get; set; }
        public string author { get; set; }
    }
}
