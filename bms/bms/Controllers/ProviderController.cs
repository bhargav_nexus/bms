﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using bms.Models;
using bms.Repository;

namespace bms.Controllers
{
    public class ProviderController : Controller
    {
        UserProfile usrrepo = new UserProfile();
        //
        // GET: /Provider/

        EmailMessage mail = new EmailMessage();
        EmailTemplateRepo tmpltrepo = new EmailTemplateRepo();

        public ActionResult Index()
        {
            FormsAuthentication.SignOut();
            return View();
        }

        [HttpPost]
        public ActionResult Index(String UserName, String Password)
        {
            if (!Roles.IsUserInRole(UserName, "provider"))
            {
                MySession.Current.MESSAGE = "Opps...Only Provider Can Login From Here";
                return View();
            }
            else
            {
                if (Membership.ValidateUser(UserName, Password))
                {
                    MembershipUser user = Membership.GetUser(UserName, true);
                    UserProfile_Prop userprofile = new UserProfile().GetRecordByUniqueId((Guid)user.ProviderUserKey);
                    MySession.Current.LOGINUSER = userprofile.upfname + " " + userprofile.uplname;
                    FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                                      1,                                     // ticket version
                                    UserName,                        // authenticated username
                                      DateTime.Now,                          // issueDate
                                      DateTime.Now.AddMinutes(20),           // expiryDate
                                      false,                          // true to persist across browser sessions
                                      user.ProviderUserKey.ToString(),                              // can be used to store additional user data
                                      FormsAuthentication.FormsCookiePath);  // the path for the cookie
                    // Encrypt the ticket using the machine key
                    string encryptedTicket = FormsAuthentication.Encrypt(ticket);
                    // Add the cookie to the request to save it
                    HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                    cookie.HttpOnly = true;
                    Response.Cookies.Add(cookie);
                    return RedirectToAction("Dashboard", "Provider");
                }
                else
                {
                    MySession.Current.MESSAGE = "Invalid Username or Password";
                    return View();
                }
            }
        }

        public ActionResult Forgot()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Forgot(String username)
        {
            MembershipUser mu = Membership.GetUser(username);
            if (mu != null)
            {
                String newpass = Utility.GetRandom();
                mu.ChangePassword(mu.GetPassword(), newpass);

                mail.mailto = mu.Email;
                mail.template = tmpltrepo.gettemplatebyname("RESETPASSWORD");
                mail.template.etemailbody = mail.template.etemailbody.Replace("{password}", newpass);
                mail.sendmail();
                if (mail.MESSAGE != null)
                {
                    MySession.Current.MESSAGE = mail.MESSAGE;
                }
                else
                {
                    MySession.Current.MESSAGE = "Your new password is sent to your registerd email address";
                }
            }
            else
            {
                MySession.Current.MESSAGE = "Your new password is sent to your registerd email address";
            }
            return View();
        }

        [Authorize(Roles = "provider")]
        public ActionResult DashBoard()
        {
            return View();
        }

        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account");
        }

        [Authorize]
        public ActionResult ChangePassword()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ChangePassword(String oldpass,String newpass,String repassword)
        {
            MembershipUser muser = Membership.GetUser();

            if (oldpass != muser.GetPassword())
            {
                MySession.Current.MESSAGE = "Please Enter Your Current Password";
                return View();
            }
            else if (newpass != repassword)
            {
                MySession.Current.MESSAGE = "Your both password do not match Please try again";
                return View();
            }
            else
            {
                muser.ChangePassword(oldpass, newpass);
                MySession.Current.MESSAGE = "Password Changed Successfully";
                FormsAuthentication.SignOut();
                return RedirectToAction("Index", "Provider");
            }
        }

        public ActionResult MyProfile()
        {
            MembershipUser mu = Membership.GetUser();
            UserProfile_Prop up = usrrepo.GetRecordByUniqueId((Guid)mu.ProviderUserKey);
            return View(up);
        }

        [HttpPost]
        public ActionResult MyProfile(UserProfile_Prop profile)
        {
            int result = usrrepo.Update(profile);
            if (result <= 0)
            {
                MySession.Current.MESSAGE = "Error :" + usrrepo.MESSAGE;
            }
            MembershipUser mu = Membership.GetUser();
            UserProfile_Prop up = usrrepo.GetRecordByUniqueId((Guid)mu.ProviderUserKey);
            MySession.Current.MESSAGE = "Profile updated successfully";
            return View(up);
        }
    }
}
