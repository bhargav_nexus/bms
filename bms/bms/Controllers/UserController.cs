﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using bms.Repository;
using bms.Models;
using System.Web.Security;

namespace bms.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        int Page = 1;
        long TotalRecs = 0;
        int PageSize = 10;
        string uname, name;
        EmailMessage mail = new EmailMessage();
        EmailTemplateRepo tmpltrepo = new EmailTemplateRepo();
        //
        // GET: /User/
        UserProfile profilerepo = new UserProfile();
        UserAddress addressreopo = new UserAddress();

        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "administrator")]
        public ActionResult Clients()
        {
            if (!String.IsNullOrEmpty(Request["pageno"])) { Page = Convert.ToInt32(Request["pageno"]); }

            if (!String.IsNullOrEmpty(Request["username"]))
            {
                uname = Request["username"];
            }
            if (!String.IsNullOrEmpty(Request["name"]))
            {
                name = Request["name"];
            }
            List<UserProfile_Prop> clients = profilerepo.GetAllRecords("user", Page, out TotalRecs, PageSize, uname, name);
            ViewBag.uname = uname;
            ViewBag.name = name;
            ViewBag.pageNo = Page;
            ViewBag.pageSize = PageSize;
            ViewBag.TotalRec = TotalRecs;
            return View(clients);
        }

        [Authorize(Roles = "administrator")]
        public ActionResult Provider()
        {
            if (!String.IsNullOrEmpty(Request["pageno"])) { Page = Convert.ToInt32(Request["pageno"]); }
            if (!String.IsNullOrEmpty(Request["username"]))
            {
                uname = Request["username"];
            }
            if (!String.IsNullOrEmpty(Request["name"]))
            {
                name = Request["name"];
            }
            List<UserProfile_Prop> providers = profilerepo.GetAllRecords("provider", Page, out TotalRecs, PageSize, uname, name);
            ViewBag.uname = uname;
            ViewBag.name = name;
            ViewBag.pageNo = Page;
            ViewBag.pageSize = PageSize;
            ViewBag.TotalRec = TotalRecs;
            return View(providers);
        }

        [Authorize(Roles = "administrator")]
        public ActionResult AddProvider()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddProvider(String UserName, UserProfile_Prop prop)
        {
            String Password = Utility.GetRandom();
            MembershipCreateStatus status;
            Membership.CreateUser(UserName, Password, UserName, "Nexus@123", "Nexus@123", true, out status);

            Roles.AddUserToRole(UserName, "provider");

            if (status == MembershipCreateStatus.Success)
            {
                prop.updob = DateTime.Now.Date;
                prop.upuserid = (Guid)Membership.GetUser(UserName).ProviderUserKey;

                int id = profilerepo.Insert(prop);
                if (id > 0)
                {
                    mail.mailto = UserName;
                    mail.template = tmpltrepo.gettemplatebyname("WELCOME");
                    mail.template.etemailbody = mail.template.etemailbody.Replace("{userName}", UserName);
                    mail.sendmail();

                    MySession.Current.MESSAGE = "Registered Successfully";
                    return RedirectToAction("Provider", "User");
                }
                else
                {
                    MySession.Current.MESSAGE = profilerepo.MESSAGE;
                    return View();
                }
            }
            else
            {
                MySession.Current.MESSAGE = "Error : " + Utility.ErrorCodeToString(status);
                return View();
            }
        }

        [Authorize(Roles = "administrator")]
        public ActionResult ChangeStatus(Guid id)
        {
            profilerepo.ChangeStatus(id);
            MySession.Current.MESSAGE = "Status Change Successfully";

            if (Request["usertype"].ToString() == "seomanager")
            {
                return RedirectToAction("SeoManager");
            }
            else if (Request["usertype"].ToString() == "provider")
            {
                return RedirectToAction("Provider");
            }
            else
            {
                return RedirectToAction("Clients");
            }
        }

        [Authorize(Roles = "administrator")]
        public ActionResult ResetPassword(Guid Id)
        {
            string newpassword = Utility.GetRandom();
            MembershipUser muser = Membership.GetUser(Id);
            muser.ChangePassword(muser.GetPassword(), newpassword);
            // send email with new password to user
            mail.mailto = muser.Email;
            mail.template = tmpltrepo.gettemplatebyname("RESETPASSWORD");
            mail.template.etemailbody = mail.template.etemailbody.Replace("{password}", newpassword);
            mail.sendmail();
            if (mail.MESSAGE != string.Empty)
            {
                MySession.Current.MESSAGE = mail.MESSAGE;
            }
            MySession.Current.MESSAGE = "Password Change Successfully";
            if (Request.QueryString["type"].ToString() == "c")
            {
                return RedirectToAction("Clients");
            }
            else
            {
                return RedirectToAction("Provider");
            }
        }

        [Authorize(Roles = "administrator")]
        public ActionResult Profile(Guid id)
        {
            UserProfile_Prop userprofile = profilerepo.GetRecordByUniqueId(id);
            UserAddress_Prop useraddress = addressreopo.GetBillingAddressUniqueId(id);
            UserAddress_Prop useraddress_ship = addressreopo.GetShippingbyUniqueId(id);
            @ViewBag.PROFILE = userprofile;
            @ViewBag.ADDRESS = useraddress;
            return View();
        }

        [Authorize(Roles = "administrator")]
        public ActionResult SeoManager()
        {
            if (!String.IsNullOrEmpty(Request["pageno"])) { Page = Convert.ToInt32(Request["pageno"]); }
            if (!String.IsNullOrEmpty(Request["username"]))
            {
                uname = Request["username"];
            }
            if (!String.IsNullOrEmpty(Request["name"]))
            {
                name = Request["name"];
            }
            List<UserProfile_Prop> managers = profilerepo.GetAllRecords("seomanager", Page, out TotalRecs, PageSize, uname, name);
            ViewBag.uname = uname;
            ViewBag.name = name;
            ViewBag.pageNo = Page;
            ViewBag.pageSize = PageSize;
            ViewBag.TotalRec = TotalRecs;
            return View(managers);
        }

        [Authorize(Roles = "administrator")]
        public ActionResult AddSeoManager()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddSeoManager(String UserName, UserProfile_Prop prop)
        {
            String Password = Utility.GetRandom();
            MembershipCreateStatus status;
            Membership.CreateUser(UserName, Password, UserName, "Nexus@123", "Nexus@123", true, out status);

            Roles.AddUserToRole(UserName, "seomanager");

            if (status == MembershipCreateStatus.Success)
            {
                prop.updob = DateTime.Now.Date;
                prop.upuserid = (Guid)Membership.GetUser(UserName).ProviderUserKey;

                int id = profilerepo.Insert(prop);
                if (id > 0)
                {
                    mail.mailto = UserName;
                    mail.template = tmpltrepo.gettemplatebyname("WELCOME");
                    mail.template.etemailbody = mail.template.etemailbody.Replace("{password}", Password);
                    mail.sendmail();

                    MySession.Current.MESSAGE = "Registered Successfully";
                    return RedirectToAction("SeoManager", "User");
                }
                else
                {
                    MySession.Current.MESSAGE = profilerepo.MESSAGE;
                    return View();
                }
            }
            else
            {
                MySession.Current.MESSAGE = "Error : " + Utility.ErrorCodeToString(status);
                return View();
            }
        }

    }
}
