﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using bms.Models;
using bms.Repository;
using Facebook;
using DotNetOpenAuth.AspNet.Clients;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;

namespace bms.Controllers
{
    public class AccountController : Controller
    {
        EmailMessage mail = new EmailMessage();
        EmailTemplateRepo tmpltrepo = new EmailTemplateRepo();
        UserProfile usrrepo = new UserProfile();
        UserAddress addressrepo = new UserAddress();

        private Uri RedirectUri
        {
            get
            {
                var uriBuilder = new UriBuilder(Request.Url);
                uriBuilder.Query = null;
                uriBuilder.Fragment = null;
                uriBuilder.Path = Url.Action("ExternalLoginCallback");
                return uriBuilder.Uri;
            }
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        public ActionResult Login(String UserName, String Password)
        {
            if (Membership.ValidateUser(UserName, Password))
            {
                MembershipUser user = Membership.GetUser(UserName, true);

                UserProfile_Prop userprofile = usrrepo.GetRecordByUniqueId((Guid)user.ProviderUserKey);
                MySession.Current.LOGINUSER = userprofile.upfname + " " + userprofile.uplname;
                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                                  1,                                     // ticket version
                                 UserName, // authenticated username
                                  DateTime.Now,                          // issueDate
                                  DateTime.Now.AddMinutes(20),           // expiryDate
                                  false,                          // true to persist across browser sessions
                                  user.ProviderUserKey.ToString(),                              // can be used to store additional user data
                                  FormsAuthentication.FormsCookiePath);  // the path for the cookie
                // Encrypt the ticket using the machine key
                string encryptedTicket = FormsAuthentication.Encrypt(ticket);
                // Add the cookie to the request to save it
                HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                cookie.HttpOnly = true;
                Response.Cookies.Add(cookie);
                return RedirectToAction("Index","Home");
            }
            else
            {
                MySession.Current.MESSAGE = "Invalid Username or Password";
                return View();
            }
        }

        //
        // GET: /Account/Register

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(String UserName,String Password,UserProfile_Prop prop)
        {
            if (UserName == string.Empty)
            {
                MySession.Current.MESSAGE = "Please enter valid email";
                return View();
            }
            if (!Utility.IsEmail(UserName))
            {
                MySession.Current.MESSAGE = "Please enter valid email";
                return View();
            }
            if (Password == string.Empty)
            {
                MySession.Current.MESSAGE = "Please enter password";
                return View();
            }
            if (prop.upfname == string.Empty || prop.upfname == null)
            {
                MySession.Current.MESSAGE = "Please enter your firstname";
                return View();
            }
            if (prop.uplname == string.Empty || prop.uplname == null)
            {
                MySession.Current.MESSAGE = "Please enter your lastname";
                return View();
            }
            if (Request["g-recaptcha-response"].ToString() == string.Empty)
            {
                MySession.Current.MESSAGE = "Please approve your self that you are not robot";
                return View();
            }
            else
            {
                MyObject data;
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(" https://www.google.com/recaptcha/api/siteverify?secret='6LdrxgwTAAAAAGDvLUFlLH8zhczrnlb2cVB-NBvo'&response=" + Request["g-recaptcha-response"].ToString());
                try
                {
                    //Google recaptcha Response
                    using (WebResponse wResponse = req.GetResponse())
                    {

                        using (StreamReader readStream = new StreamReader(wResponse.GetResponseStream()))
                        {
                            string jsonResponse = readStream.ReadToEnd();

                            JavaScriptSerializer js = new JavaScriptSerializer();
                            data = js.Deserialize<MyObject>(jsonResponse);// Deserialize Json
                        }
                    }
                }
                catch (WebException ex)
                {
                    throw ex;
                }
                if (!Convert.ToBoolean(data.success))
                {
                    MySession.Current.MESSAGE = "Invalid captcha Please try agian";
                    return View();
                }
            }
            if (prop.updob == DateTime.MinValue || prop.updob == null)
            {
                prop.updob = null;
            }
            if (prop.upcontact == null)
            {
                prop.upcontact = string.Empty;
            }

            MembershipCreateStatus status;
            Membership.CreateUser(UserName, Password, UserName, "Nexus@123", "Nexus@123", true, out status);
            if (status == MembershipCreateStatus.Success)
            {
                Roles.AddUserToRole(UserName, "user");
                prop.updob = DateTime.Now.Date;
                prop.upuserid = (Guid)Membership.GetUser(UserName).ProviderUserKey;
                UserProfile profilerepo = new UserProfile();

                int id = profilerepo.Insert(prop);
                MySession.Current.LOGINUSER = prop.upfname + " " + prop.uplname;
                if (id > 0)
                {
                    mail.mailto = UserName;
                    mail.template = tmpltrepo.gettemplatebyname("WELCOME");
                    mail.template.etemailbody = mail.template.etemailbody.Replace("{userName}", UserName);
                    mail.sendmail();
                    if (mail.MESSAGE != string.Empty)
                    {
                        MySession.Current.MESSAGE = mail.MESSAGE;
                    }

                    MembershipUser user = Membership.GetUser(UserName);

                    FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                                      1,                                     // ticket version
                                     UserName,                        // authenticated username
                                      DateTime.Now,                          // issueDate
                                      DateTime.Now.AddMinutes(20),           // expiryDate
                                      false,                          // true to persist across browser sessions
                                      user.ProviderUserKey.ToString(),                              // can be used to store additional user data
                                      FormsAuthentication.FormsCookiePath);  // the path for the cookie
                    // Encrypt the ticket using the machine key
                    string encryptedTicket = FormsAuthentication.Encrypt(ticket);
                    // Add the cookie to the request to save it
                    HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                    cookie.HttpOnly = true;
                    Response.Cookies.Add(cookie);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    MySession.Current.MESSAGE = profilerepo.MESSAGE;
                    return View();
                }
            }
            else
            {
                MySession.Current.MESSAGE =  Utility.ErrorCodeToString(status);
                return View();
            }
        }

        // POST: /Account/LogOff
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }


        public ActionResult MyProfile()
        {
            MembershipUser mu = Membership.GetUser();
            UserProfile_Prop up = usrrepo.GetRecordByUniqueId((Guid)mu.ProviderUserKey);
            UserAddress_Prop ua = addressrepo.GetRecordByUniqueId((Guid)mu.ProviderUserKey);
            @ViewBag.PROFILE = up;
            @ViewBag.ADDRESS =ua ;

            return View();
        }

        [HttpPost]
        public ActionResult MyProfile(UserProfile_Prop profile,UserAddress_Prop address)
        {
            if (profile.upfname == string.Empty || profile.upfname == null)
            {
                MySession.Current.MESSAGE = "Please enter your firstname";
                return View();
            }
            if (profile.uplname == string.Empty || profile.uplname == null)
            {
                MySession.Current.MESSAGE = "Please enter your lastname";
                return View();
            }
            if (profile.updob == DateTime.MinValue || profile.updob == null)
            {
                profile.updob = null;
            }
            if (profile.upcontact == null)
            {
                profile.upcontact = string.Empty;
            }
            int result = usrrepo.Update(profile);
            if (result <= 0)
            {
                MySession.Current.MESSAGE = "Error :" + usrrepo.MESSAGE;
            }

            if (address.uauserid == new Guid())
            {
                address.uauserid = profile.upuserid;
            }
            address.uatype = true;
            address.uastatus = true;
            result = addressrepo.Insert(address);
            if (result > 0)
            {
                MySession.Current.MESSAGE = "User Profile Updated Successfully...!";
            }
            MembershipUser mu = Membership.GetUser();
            UserProfile_Prop up = usrrepo.GetRecordByUniqueId((Guid)mu.ProviderUserKey);
            UserAddress_Prop ua = addressrepo.GetRecordByUniqueId((Guid)mu.ProviderUserKey);
            @ViewBag.PROFILE = up;
            @ViewBag.ADDRESS = ua;
            return View();
        }


        public ActionResult ChangePassword()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ChangePassword(String oldpass, String newpass, String repassword)
        {
            MembershipUser muser = Membership.GetUser();

            if (oldpass != muser.GetPassword())
            {
                MySession.Current.MESSAGE = "Please Enter Your Current Password";
                return View();
            }
            else if (newpass != repassword)
            {
                MySession.Current.MESSAGE = "Your both password do not match Please try again";
                return View();
            }
            else
            {
                muser.ChangePassword(oldpass, newpass);
                MySession.Current.MESSAGE = "Password Changed Successfully,Please Login Again to Continue";
                FormsAuthentication.SignOut();
                return RedirectToAction("Login", "Account");
            }
        }

        public ActionResult Forgot()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Forgot(String username)
        {
            MembershipUser mu = Membership.GetUser(username);
            if (mu != null)
            {
                String newpass = Utility.GetRandom();
                mu.ChangePassword(mu.GetPassword(), newpass);

                mail.mailto = mu.Email;
                mail.template = tmpltrepo.gettemplatebyname("RESETPASSWORD");
                mail.template.etemailbody = mail.template.etemailbody.Replace("{password}", newpass);
                mail.sendmail();
                if (mail.MESSAGE != null)
                {
                    MySession.Current.MESSAGE = mail.MESSAGE;
                }
                else
                {
                    MySession.Current.MESSAGE = "Your new password is sent to your registerd email address";
                }
            }
            else
            {
                MySession.Current.MESSAGE = "Your new password is sent to your registerd email address";
            }
            return View();
        }

        #region
        [HttpPost]
        public ActionResult ExternalLogin()
        {
            var fb = new Facebook.FacebookClient();
            var loginUrl = fb.GetLoginUrl(new
            {
                client_id = "452415551612901",
                client_secret = "ce56802e633062c142a02b6a0b6346d5",
                redirect_uri = RedirectUri.AbsoluteUri,
                response_type = "code",
                scope = "email" // Add other permissions as needed
            });

            return Redirect(loginUrl.AbsoluteUri);
        }

        public ActionResult ExternalLoginCallback(string code)
        {
            if (code != null)
            {
                var fb = new Facebook.FacebookClient(code);
                dynamic result = fb.Post("oauth/access_token", new
                {
                    client_id = "452415551612901",
                    client_secret = "ce56802e633062c142a02b6a0b6346d5",
                    redirect_uri = RedirectUri.AbsoluteUri,
                    code = code
                });
                string accessToken = result.access_token;
                if (result.access_token != null)
                {
                    fb.AccessToken = accessToken;
                    dynamic me = fb.Get("me?fields=first_name,last_name,id,email");
                    string email = me.email;
                    MembershipUser mu = Membership.GetUser(email);
                    if (mu == null)
                    {
                        ViewBag.fbemail = email;
                        ViewBag.fbfname = me.first_name;
                        ViewBag.fblname=me.last_name;
                        return View();
                    }
                    else
                    {
                        MembershipUser user = Membership.GetUser(email, true);
                        UserProfile_Prop userprofile = usrrepo.GetRecordByUniqueId((Guid)user.ProviderUserKey);
                        MySession.Current.LOGINUSER = userprofile.upfname + " " + userprofile.uplname;
                        FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                                              1,                                     // ticket version
                                             email,                      // authenticated username
                                              DateTime.Now,                          // issueDate
                                              DateTime.Now.AddMinutes(60),           // expiryDate
                                              false,                          // true to persist across browser sessions
                                              user.ProviderUserKey.ToString(),                              // can be used to store additional user data
                                              FormsAuthentication.FormsCookiePath);  // the path for the cookie
                        // Encrypt the ticket using the machine key
                        string encryptedTicket = FormsAuthentication.Encrypt(ticket);
                        // Add the cookie to the request to save it
                        HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                        cookie.HttpOnly = true;
                        Response.Cookies.Add(cookie);
                        return RedirectToAction("Index", "Home");
                    }
                }
            }
            else
            {
                MySession.Current.MESSAGE = "Unable to Authenticate Your Account";
                return RedirectToAction("Registe", "Account");
            }
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult ExternalLoginCallback(String UserName, String Password, UserProfile_Prop prop)
        {
            try
            {
                MembershipCreateStatus status;
                Membership.CreateUser(UserName, Password, UserName, "Nexus@123", "Nexus@123", true, out status);
                if (status == MembershipCreateStatus.Success)
                {
                    prop.updob = DateTime.Now.Date;
                    prop.upuserid = (Guid)Membership.GetUser(UserName).ProviderUserKey;
                    UserProfile profilerepo = new UserProfile();
                    int id = profilerepo.Insert(prop);
                    if (id > 0)
                    {
                        MySession.Current.LOGINUSER = prop.upfname + " " + prop.uplname;
                        MySession.Current.MESSAGE = "Registered Successfully";
                        Roles.AddUserToRole(UserName, "user");
                        MembershipUser user = Membership.GetUser(UserName, true);
                        FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                                              1,                                     // ticket version
                                            UserName,                        // authenticated username
                                              DateTime.Now,                          // issueDate
                                              DateTime.Now.AddMinutes(60),           // expiryDate
                                              false,                          // true to persist across browser sessions
                                              user.ProviderUserKey.ToString(),                              // can be used to store additional user data
                                              FormsAuthentication.FormsCookiePath);  // the path for the cookie
                        // Encrypt the ticket using the machine key
                        string encryptedTicket = FormsAuthentication.Encrypt(ticket);
                        // Add the cookie to the request to save it
                        HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                        cookie.HttpOnly = true;
                        Response.Cookies.Add(cookie);
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        MySession.Current.MESSAGE = profilerepo.MESSAGE;
                        return View();
                    }
                }
                else
                {
                    MySession.Current.MESSAGE = Utility.ErrorCodeToString(status);
                    return View();
                }
            }
            catch (Exception ex)
            {
                ViewBag.fbemail = UserName;
                MySession.Current.MESSAGE = ex.Message;
                return View();
            }
        }
        #endregion

        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        #endregion
    }
}
