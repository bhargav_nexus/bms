﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace bms.Controllers
{
     [Authorize(Roles = "user")]
    public class CartController : Controller
    {
        //
        // GET: /Cart/
       
        public ActionResult MyOrders()
        {
            return View();
        }

        public ActionResult MyCart()
        {
            return View();
        }
    }
}
