﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using bms.Models;
using bms.Repository;

namespace bms.Controllers
{
    [Authorize(Roles = "administrator")]
    public class MasterController : Controller
    {
        //
        // GET: /Master/

        #region Tags
        tag tagrepo = new tag();
        public ActionResult TagMaster()
        {
            List<tag_Prop> taglist = tagrepo.GetAllRecords();
            return View(taglist);
        }
        public ActionResult AddTag()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddTag(tag_Prop prop)
        {
            prop.status = true;
            int i = tagrepo.Insert(prop);
            if (i > 0)
            {
                MySession.Current.MESSAGE = "Tag Insered Successfully";
                return RedirectToAction("TagMaster");
            }
            else
            {
                MySession.Current.MESSAGE = tagrepo.MESSAGE;
                return View();
            }
        }

        public ActionResult ChangeTagStatus(Int64 id)
        {
            tagrepo.ChangeStatus(id);
            MySession.Current.MESSAGE = tagrepo.MESSAGE;
            return RedirectToAction("TagMaster", "Master");
        }
        #endregion

        #region Event
        eventRepo eventrepo = new eventRepo();

        public ActionResult EventMaster()
        {
            List<event_Prop> eventlist = eventrepo.GetAllRecords();
            return View(eventlist);
        }

        public ActionResult AddEvent()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddEvent(event_Prop prop)
        {
            prop.status = true;
            int i = eventrepo.Insert(prop);
            if (i > 0)
            {
                MySession.Current.MESSAGE = "Event Added Successfully";
                return RedirectToAction("EventMaster");
            }
            else
            {
                MySession.Current.MESSAGE = eventrepo.MESSAGE;
                return View();
            }
        }

        [HttpPost]
        public ActionResult ChangeEventStatus(Int64 id)
        {
            Boolean blnRslt = eventrepo.ChangeStatus(id);
            MySession.Current.MESSAGE = eventrepo.MESSAGE;
            return RedirectToAction("EventMaster");
        }
        #endregion

        #region Sportperson
        sportperson sportpersonrepo = new sportperson();

        public ActionResult SportPerson()
        {
            List<sportperson_Prop> sportpersonlist = sportpersonrepo.GetAllRecords();
            return View(sportpersonlist);
        }

        public ActionResult AddSportPerson()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddSportPerson(sportperson_Prop prop)
        {
            prop.status = true;
            int i = sportpersonrepo.Insert(prop);
            if (i > 0)
            {
                MySession.Current.MESSAGE = "SportPerson Added Successfully";
                return RedirectToAction("SportPerson");
            }
            else
            {
                MySession.Current.MESSAGE = eventrepo.MESSAGE;
                return View();
            }
        }

        public ActionResult ChangeSportPersonStatus(Int64 id)
        {
            Boolean blnRslt = sportpersonrepo.ChangeStatus(id);
            MySession.Current.MESSAGE = sportpersonrepo.MESSAGE;
            return RedirectToAction("SportPerson");
        }
        #endregion

        #region Imgcategory
        category categoryrepo = new category();
        public ActionResult CategoryMaster()
        {
            List<category_Prop> catlist = categoryrepo.GetAllRecords();
            return View(catlist);
        }

        public ActionResult AddCategory()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddCategory(category_Prop prop)
        {
            prop.status = true;
            int i = categoryrepo.Insert(prop);
            if (i > 0)
            {
                MySession.Current.MESSAGE = "Category Insered Successfully";
                return RedirectToAction("CategoryMaster");
            }
            else
            {
                MySession.Current.MESSAGE = categoryrepo.MESSAGE;
                return View();
            }
        }

        public ActionResult ChangeCategoryStatus(Int64 id)
        {
            categoryrepo.ChangeStatus(id);
            MySession.Current.MESSAGE = categoryrepo.MESSAGE;
            return RedirectToAction("CategoryMaster", "Master");
        }
        #endregion

        #region imgtype
        types typereop = new types();
        public ActionResult TypeMaster()
        {
            List<types_Prop> typelist = typereop.GetAllRecords();
            return View(typelist);
        }

        public ActionResult EditType(Int64 id)
        {
            types_Prop type = typereop.GetRecordById(id);
            return View();
        }

        [HttpPost]
        public ActionResult EditType(types_Prop prop)
        {
            int i = typereop.Insert(prop);
            if (i > 0)
            {
                MySession.Current.MESSAGE = "Type Updated Successfully";
                return RedirectToAction("TypeMaster");
            }
            else
            {
                MySession.Current.MESSAGE = typereop.MESSAGE;
                return View();
            }
        }
        #endregion

        #region place
        places placerepo = new places();
        public ActionResult PlaceMaster()
        {
            List<places_Prop> placelist = placerepo.GetAllRecords();
            return View(placelist);
        }
        public ActionResult AddPlace()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddPlace(places_Prop prop)
        {
            prop.pstatus = true;
            int i = placerepo.Insert(prop);
            if (i > 0)
            {
                MySession.Current.MESSAGE = "Place Insered Successfully";
                return RedirectToAction("PlaceMaster");
            }
            else
            {
                MySession.Current.MESSAGE = placerepo.MESSAGE;
                return View();
            }
        }
        public ActionResult ChangePlaceStatus(Int64 id)
        {
            placerepo.ChangeStatus(id);
            MySession.Current.MESSAGE = placerepo.MESSAGE;
            return RedirectToAction("PlaceMaster", "Master");
        }
        #endregion

        #region years
        year yearrepo = new year();
        public ActionResult YearMaster()
        {
            List<year_Prop> yearlist = yearrepo.GetAllRecords();
            return View(yearlist);
        }
        public ActionResult AddYear()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddYear(year_Prop prop)
        {
            prop.status = true;
            int i = yearrepo.Insert(prop);
            if (i > 0)
            {
                MySession.Current.MESSAGE = "Year Insered Successfully";
                return RedirectToAction("YearMaster");
            }
            else
            {
                MySession.Current.MESSAGE = placerepo.MESSAGE;
                return View();
            }
        }
        public ActionResult ChangeYearStatus(Int64 id)
        {
            yearrepo.ChangeStatus(id);
            MySession.Current.MESSAGE = yearrepo.MESSAGE;
            return RedirectToAction("YearMaster", "Master");
        }
        #endregion

    }
}
