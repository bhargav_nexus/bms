﻿using System;
using System.IO;
using bms.Repository;
using bms.Models;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
//using LevDan.Exif;

namespace bms.Upload
{
    public class FilesStatus
    {
        types typerepo = new types();
        imageresources imageresourcesrepo = new imageresources();

        public const string HandlerPath = "/Upload/";

        public string group { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public int size { get; set; }
        public string progress { get; set; }
        public string url { get; set; }
        public string thumbnail_url { get; set; }
        public string delete_url { get; set; }
        public string delete_type { get; set; }
        public string error { get; set; }
        public string resolutionstatus { get; set; }

        public FilesStatus() { }

        public FilesStatus(FileInfo fileInfo, int resid, bool check)
        {
            SetValues(fileInfo.Name, (int)fileInfo.Length, fileInfo.FullName, resid, check);
            //validateimage(fileInfo.FullName);
        }

        public FilesStatus(string fileName, int fileLength, string fullPath, int resid, bool check)
        {
            SetValues(fileName, fileLength, fullPath, resid, check);
        }

        private void SetValues(string fileName, int fileLength, string fullPath, int resid, bool chk)
        {
            name = fileName;
            type = "image/png";
            size = fileLength;
            progress = "1.0";
            url = HandlerPath + "UploadHandler.ashx?f=" + fileName;
            delete_url = HandlerPath + "UploadHandler.ashx?f=" + fileName;
            delete_type = "DELETE";
            var ext = Path.GetExtension(fullPath);
            var fileSize = ConvertBytesToMegabytes(new FileInfo(fullPath).Length);
            if (fileSize > 3 || !IsImage(ext)) thumbnail_url = "/Uploader/img/generalFile.png";
            else thumbnail_url = @"data:image/png;base64," + EncodeFile(fullPath);
            if (chk)
            {
                int id = validateimage(fullPath);
                if (id > 0)
                {
                    resolutionstatus = true.ToString().ToLower();
                    imageresources_Prop resprop = new imageresources_Prop();
                    resprop.imgid = resid;
                    resprop.imgname = fileName;
                    resprop.imgtype = id;
                    resprop.imgsize = (double)fileSize;
                    resprop.imgthumbnail = thumbnail_url;
                    int rid = imageresourcesrepo.Insert(resprop);
                    delete_url = HandlerPath + "UploadHandler.ashx?f=" + fileName + "&r=" + rid;
                }
                else
                {
                    resolutionstatus = false.ToString().ToLower();
                }
            }
            else
            {
                //save prodcut images
                productimages prdct = new productimages();
                prdct.productid = resid;
                prdct.productimgname = fileName;
                int retid = new productsRepo().InsertProductImage(prdct);
                resolutionstatus = true.ToString().ToLower();
            }
        }

        private bool IsImage(string ext)
        {
            return ext == ".jpg" || ext == ".png";
        }

        private string EncodeFile(string fileName)
        {
            return Convert.ToBase64String(System.IO.File.ReadAllBytes(fileName));
        }

        static double ConvertBytesToMegabytes(long bytes)
        {
            return (bytes / 1024f) / 1024f;
        }

        // created by admin
        private int validateimage(String filename)
        {
            int typeid = 0;
            Image img = new Bitmap(filename);
            long width = img.Width;
            long height = img.Height;
            typeid = typerepo.gettypebyresolution(height, width);


            //ExifTagCollection exif = new ExifTagCollection(filename);
            //foreach (ExifTag tag in exif)
            //{
            //    Console.Out.WriteLine(tag);
            //}

            var propitems = img.PropertyItems;

            foreach (var p in propitems)
            {
                if (p.Id == 0x010E)
                {
                    string tag = p.Value.ToString();
                }
            }

            return typeid;
        }
    }
}