﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bms.Models
{
     [PetaPoco.TableName("image"), PetaPoco.PrimaryKey("imgid")]
    public class image_Prop
    {
        public long imgid { get; set; }

        public System.Guid imguser { get; set; }

        public string imgname { get; set; }

        public string imgdetail { get; set; }

        public bool status { get; set; }

        public DateTime imgcaptureddate { get; set; }

        public DateTime imginsertdate { get; set; }
    }
}