﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bms.Models
{
    [PetaPoco.TableName("year"), PetaPoco.PrimaryKey("yid")]
    public class year_Prop
    {
        public long yid { get; set; }

        public string yname { get; set; }

        public bool status { get; set; }
    }
}