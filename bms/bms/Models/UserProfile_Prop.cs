﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bms.Models
{
    [PetaPoco.TableName("UserProfile"), PetaPoco.PrimaryKey("upid")]
    public class UserProfile_Prop
    {
        public long upid { get; set; }

        public System.Guid upuserid { get; set; }

        public string upfname { get; set; }

        public string uplname { get; set; }

        public System.DateTime? updob { get; set; }

        public int upgender { get; set; }

        public string upcontact { get; set; }

        [ResultColumn]
        public string UserName { get; set; }
        [ResultColumn]
        public bool Status { get; set; }

        [ResultColumn]
        public DateTime CreateDate { get; set; }
        [ResultColumn]
        public DateTime LastLoginDate { get; set; }
    }
}