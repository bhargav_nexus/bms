﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bms.Models
{
    [PetaPoco.TableName("types"), PetaPoco.PrimaryKey("tid")]
    public class types_Prop
    {
        public long tid { get; set; }

        public string ttype { get; set; }

        public string tresolution { get; set; }

        public int theight { get; set; }

        public int twidth { get; set; }

        public float thightinch { get; set; }

        public float twidthinch { get; set; }
    }
}