﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bms.Models
{
     [PetaPoco.TableName("imageeventmapping"), PetaPoco.PrimaryKey("ieid")]
    public class imageeventmapping_Prop
    {
        public long ieid { get; set; }

        public long ieimageid { get; set; }

        public long ieeventid { get; set; } 
    }
}