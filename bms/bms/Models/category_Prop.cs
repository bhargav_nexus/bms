﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bms.Models
{
    [PetaPoco.TableName("category"), PetaPoco.PrimaryKey("catid")]
    public class category_Prop
    {
        public long catid { get; set; }

        public string catname { get; set; }

        public bool status { get; set; } 
    }
}