﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bms.Models
{
     [PetaPoco.TableName("event"), PetaPoco.PrimaryKey("eid")]
    public class event_Prop
    {
        public long eid { get; set; }

        public string ename { get; set; }

        public string description { get; set; }

        public string year { get; set; }

        public bool status { get; set; } 
    }
}   