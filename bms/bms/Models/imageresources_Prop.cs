﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bms.Models
{
    [PetaPoco.TableName("imageresources"), PetaPoco.PrimaryKey("id")]
    public class imageresources_Prop
    {
        public long id { get; set; }

        public long imgid { get; set; }

        public string imgname { get; set; }

        public long imgtype { get; set; }

        public double imgsize { get; set; }

        public string imgthumbnail { get; set; }
    }
}