﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bms.Models
{
     [PetaPoco.TableName("places"), PetaPoco.PrimaryKey("pid")]
    public class places_Prop
    {
        public long pid { get; set; }

        public string pname { get; set; }

        public bool pstatus { get; set; }
    }
}