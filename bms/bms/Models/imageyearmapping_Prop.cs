﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bms.Models
{
    [PetaPoco.TableName("imageyearmapping"), PetaPoco.PrimaryKey("iyid")]
    public class imageyearmapping_Prop
    {
        public long iyid { get; set; }

        public long iyimageid { get; set; }

        public long iyyearid { get; set; } 
    }
}