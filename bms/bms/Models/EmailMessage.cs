﻿using bms.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace bms.Models
{
    public class EmailMessage
    {
        public emailtemplateModel template { get; set; }
        public String mailto { get; set; }

        public String MESSAGE { get; set; }

        public void sendmail()
        {
            // getting account associted with template
            template.emaccount = new EmailAccountRepo().getaccount(template.eteaid);

            // configuring mail
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(template.emaccount.eaemail);
            mail.To.Add(this.mailto);
            mail.Subject = template.etsubject;
            mail.Body = template.etemailbody;
            mail.IsBodyHtml = true;
            // configuring smtp server
            SmtpClient SmtpServer = new SmtpClient(template.emaccount.easmtpserver);
            SmtpServer.UseDefaultCredentials = true;
            SmtpServer.Credentials = new NetworkCredential(template.emaccount.eaemail, template.emaccount.eapassword);
            // sending mail
            try
            {
                SmtpServer.Send(mail);
            }
            catch(Exception ex)
            {
                MESSAGE = ex.Message;
            }
        }
    }
}