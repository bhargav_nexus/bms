﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bms.Models
{
    [PetaPoco.TableName("tag"), PetaPoco.PrimaryKey("tid")]
    public class tag_Prop
    {
        public long tid { get; set; }

        public string tname { get; set; }

        public bool status { get; set; }  
    }
}