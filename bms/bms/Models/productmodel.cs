﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
using System.ComponentModel.DataAnnotations;
namespace bms.Models
{
    #region productmodel
    [PetaPoco.TableName("products"), PetaPoco.PrimaryKey("productid")]
    public class products
    {
        [Required]
        public int productid { get; set; }
        [Required]
        public string productname { get; set; }
        [Required]
        public string productshortdesc { get; set; }
        [Required]
        public string productdesc { get; set; }
        [ResultColumn]
        public int catid { get; set; }
        [ResultColumn]
        public List<int> tagslst { get; set; }
    }
    #endregion

    #region productiamges
    [PetaPoco.TableName("productimages"), PetaPoco.PrimaryKey("porductimgid")]
    public class productimages
    {
        [Required]
        public int productimgid { get; set; }
        [Required]
        public int productid { get; set; }
        [Required]
        public string productimgname { get; set; }
        [Required]
        public string prdctnewimgname { get; set; }
    }
    #endregion

    #region producttagmodel
    [PetaPoco.TableName("producttags"), PetaPoco.PrimaryKey("tagid")]
    public class producttags
    {
        [Required]
        public int tagid { get; set; }
        [Required]
        public string tagname { get; set; }
    }
    #endregion

    #region producttagmapping
    [PetaPoco.TableName("producttagmapping"), PetaPoco.PrimaryKey("ptmapid")]
    public class producttagmapping
    {
        [Required]
        public int ptmapid { get; set; }
        [Required]
        public int prdctid { get; set; }
        [Required]
        public int tagid { get; set; }
    }
    #endregion

    #region productcatagorymodel
    [PetaPoco.TableName("productcategory"), PetaPoco.PrimaryKey("productcatid")]
    public class productcategory
    {
        [Required]
        public int productcatid { get; set; }
        [Required]
        public string productcatname { get; set; }
        [Required]
        public bool catstatus { get; set; }
    }
    #endregion

    #region productcatmapping
    [PetaPoco.TableName("productcatmapping"), PetaPoco.PrimaryKey("pcmapid")]
    public class productcatmapping
    {
        [Required]
        public int pcmapid { get; set; }
        [Required]
        public int prdctid { get; set; }
        [Required]
        public int catid { get; set; }
    }
    #endregion
}