﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bms.Models
{
     [PetaPoco.TableName("sportperson"), PetaPoco.PrimaryKey("sid")]
    public class sportperson_Prop
    {
        public long sid { get; set; }

        public string sname { get; set; }
        public string sdescription { get; set; }

        public bool status { get; set; } 
    }
}