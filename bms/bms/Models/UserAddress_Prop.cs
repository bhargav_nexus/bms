﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bms.Models
{
     [PetaPoco.TableName("UserAddress"), PetaPoco.PrimaryKey("uaid")]
    public class UserAddress_Prop
    {
        public long uaid { get; set; }

        public System.Guid uauserid { get; set; }

        public string uaaddress { get; set; }

        public string uacity { get; set; }

        public string uastate { get; set; }

        public string uapincode { get; set; }

        public bool uatype { get; set; }

        public bool uastatus { get; set; }
    }
}