﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace bms.Models
{
    [PetaPoco.TableName("emailtemplateMaster"), PetaPoco.PrimaryKey("etid")]
    public class emailtemplateModel
    {
        public Int64 etid { get; set; }
        public Int64 eteaid { get; set; }
        public String etname { get; set; }
        public String etsubject { get; set; }
        public String etemailbody { get; set; }
        public String etbccemail { get; set; }
        public String etccemail { get; set; }
        public Boolean etstatus { get; set; }
        [ResultColumn]
        public emailaccountModel emaccount { get; set; }
    }
}