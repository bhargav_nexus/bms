﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bms.Models
{
     [PetaPoco.TableName("imagetagmapping"), PetaPoco.PrimaryKey("itid")]
    public class imagetagmapping_Prop
    {
        public long itid { get; set; }

        public long itimageid { get; set; }

        public long ittagid { get; set; } 
    }
}