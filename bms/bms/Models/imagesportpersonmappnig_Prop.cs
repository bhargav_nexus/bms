﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bms.Models
{
     [PetaPoco.TableName("imagesportpersonmappnig"), PetaPoco.PrimaryKey("isid")]
    public class imagesportpersonmappnig_Prop
    {
        public long isid { get; set; }

        public long isimageid { get; set; }

        public long issportpersonid { get; set; }       
    }
}