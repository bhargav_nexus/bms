﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace bms.Models
{
    [PetaPoco.TableName("emailaccountMaster"), PetaPoco.PrimaryKey("eaid")]
    public class emailaccountModel
    {
        public Int64 eaid { get; set; }
        public String eaname { get; set; }
        public String eaemail { get; set; }
        public String eapassword { get; set; }
        public String easmtpserver { get; set; }
        public Boolean eaenablessl { get; set; }
    }
}