﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using bms.Models;

namespace bms.ViewModels
{
    public class imageView
    {
        public long imgid { get; set; }

        public System.Guid imguser { get; set; }

        public string imgname { get; set; }

        public string imgdetail { get; set; }

        public bool status { get; set; }

        public DateTime imgcaptureddate { get; set; }

        public DateTime imginsertdate { get; set; }

        public string tag { get; set; }

        public string ename { get; set; }

        public string pname { get; set; }

        public string sname { get; set; }
    }
}