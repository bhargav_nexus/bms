﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bms.ViewModels
{
    public class ProductsViewModel
    {
        public int productid { get; set; }
        public string productname { get; set; }
        public string productshortdesc { get; set; }
        public string productdesc { get; set; }
        public string tagname { get; set; }
        public string productcatname { get; set; }
    }
}